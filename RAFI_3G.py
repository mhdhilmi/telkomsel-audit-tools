import fitting

# Param 3G
def user_hsdpa():
    user_hsdpa = fitting.fitting_template_3G()
    NodeBLocalCell = fitting.mo_read("NodeBLocalCell")
    UtranCell = fitting.mo_read("UtranCell")
    NodeBFunction = fitting.mo_read("NodeBFunction")
    CapacityState = fitting.mo_filter("CapacityState", "CXC4021002")
    CapacityState1 = fitting.mo_filter("CapacityState", "CXC4021007")
    
    #NodeBLocalCell
    dict_fitur = NodeBLocalCell.set_index('CellId').to_dict()['maxNumHsdpaUsers']
    user_hsdpa['maxNumHsdpaUsers'] = user_hsdpa['Cellid'].map(dict_fitur)

    dict_fitur = NodeBLocalCell.set_index('CellId').to_dict()['maxNumHsdpaUsers_recomend']
    user_hsdpa['maxNumHsdpaUsers_recomend'] = user_hsdpa['Cellid'].map(dict_fitur)
    
    dict_fitur = NodeBLocalCell.set_index('CellId').to_dict()['maxNumEulUsers']
    user_hsdpa['maxNumEulUsers'] = user_hsdpa['Cellid'].map(dict_fitur)

    dict_fitur = NodeBLocalCell.set_index('CellId').to_dict()['maxNumEulUsers_recomend']
    user_hsdpa['maxNumEulUsers_recomend'] = user_hsdpa['Cellid'].map(dict_fitur)
    
    #UtranCell
    dict_fitur = UtranCell.set_index('CellId').to_dict()['hsdpaUsersAdm']
    user_hsdpa['hsdpaUsersAdm'] = user_hsdpa['Cellid'].map(dict_fitur)

    dict_fitur = UtranCell.set_index('CellId').to_dict()['hsdpaUsersAdm_recomend']
    user_hsdpa['hsdpaUsersAdm_recomend'] = user_hsdpa['Cellid'].map(dict_fitur)
    
    dict_fitur = UtranCell.set_index('CellId').to_dict()['eulServingCellUsersAdm']
    user_hsdpa['eulServingCellUsersAdm'] = user_hsdpa['Cellid'].map(dict_fitur)

    dict_fitur = UtranCell.set_index('CellId').to_dict()['eulServingCellUsersAdm_recomend']
    user_hsdpa['eulServingCellUsersAdm_recomend'] = user_hsdpa['Cellid'].map(dict_fitur)
    
    #NodeBFunction
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['licenseStateNumHsdpaUsers']
    user_hsdpa['licenseStateNumHsdpaUsers'] = user_hsdpa['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['licenseStateNumHsdpaUsers_recomend']
    user_hsdpa['licenseStateNumHsdpaUsers_recomend'] = user_hsdpa['Siteid'].map(dict_fitur)
    
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['licenseStateNumEulUsers']
    user_hsdpa['licenseStateNumEulUsers'] = user_hsdpa['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['licenseStateNumEulUsers_recomend']
    user_hsdpa['licenseStateNumEulUsers_recomend'] = user_hsdpa['Siteid'].map(dict_fitur)
    
    #CapacityState
    dict_fitur = CapacityState.set_index('SiteID').to_dict()['currentCapacityLimit_value']
    user_hsdpa['CXC4021007'] = user_hsdpa['Siteid'].map(dict_fitur)

    dict_fitur = CapacityState.set_index('SiteID').to_dict()['currentCapacityLimit_value_recomend']
    user_hsdpa['CXC4021007_recomend'] = user_hsdpa['Siteid'].map(dict_fitur)
    
    dict_fitur = CapacityState1.set_index('SiteID').to_dict()['currentCapacityLimit_value']
    user_hsdpa['CXC4021002'] = user_hsdpa['Siteid'].map(dict_fitur)

    dict_fitur = CapacityState1.set_index('SiteID').to_dict()['currentCapacityLimit_value_recomend']
    user_hsdpa['CXC4021002_recomend'] = user_hsdpa['Siteid'].map(dict_fitur)
    
    user_hsdpa = fitting.check(user_hsdpa)
    return user_hsdpa

def dl16qam():
    dl16qam = fitting.fitting_template_3G()
    NodeBFunction = fitting.mo_read("NodeBFunction")
    fitur = fitting.mo_filter("FeatureState", "CXC4020002")
    
    #FeatureState
    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState']
    dl16qam['CXC4020002'] = dl16qam['Siteid'].map(dict_fitur)

    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState_recomend']
    dl16qam['CXC4020002_recomend'] = dl16qam['Siteid'].map(dict_fitur)
    
    #NodeBFunction
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureState16Qam']
    dl16qam['featureState16Qam'] = dl16qam['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureState16Qam_recomend']
    dl16qam['featureState16Qam_recomend'] = dl16qam['Siteid'].map(dict_fitur)
    
    dl16qam = fitting.state(dl16qam, "CXC4020002", "featureState16Qam")
    dl16qam = fitting.check(dl16qam)
    return dl16qam

def dl64qam():
    dl64qam = fitting.fitting_template_3G()
    NodeBFunction = fitting.mo_read("NodeBFunction")
    fitur = fitting.mo_filter("FeatureState", "CXC4020014")
    
    #FeatureState
    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState']
    dl64qam['CXC4020014'] = dl64qam['Siteid'].map(dict_fitur)

    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState_recomend']
    dl64qam['CXC4020014_recomend'] = dl64qam['Siteid'].map(dict_fitur)
    
    #NodeBFunction
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureState64Qam']
    dl64qam['featureState64Qam'] = dl64qam['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureState64Qam_recomend']
    dl64qam['featureState64Qam_recomend'] = dl64qam['Siteid'].map(dict_fitur)
    
    dl64qam = fitting.state(dl64qam, "CXC4020014", "featureState64Qam")
    dl64qam = fitting.check(dl64qam)
    return dl64qam

def eultti():
    eultti = fitting.fitting_template_3G()
    NodeBFunction = fitting.mo_read("NodeBFunction")
    fitur = fitting.mo_filter("FeatureState", "CXC4020011")
    
    #FeatureState
    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState']
    eultti['CXC4020011'] = eultti['Siteid'].map(dict_fitur)

    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState_recomend']
    eultti['CXC4020011_recomend'] = eultti['Siteid'].map(dict_fitur)
    
    #NodeBFunction
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateEul2msTti']
    eultti['featureStateEul2msTti'] = eultti['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateEul2msTti_recomend']
    eultti['featureStateEul2msTti_recomend'] = eultti['Siteid'].map(dict_fitur)
    
    eultti = fitting.state(eultti, "CXC4020011", "featureStateEul2msTti")
    eultti = fitting.check(eultti)
    return eultti

def eulmc():
    eulmc = fitting.fitting_template_3G()
    NodeBLocalCell = fitting.mo_read("NodeBLocalCell")
    RbsLocalCell = fitting.mo_read("RbsLocalCell")
    fitur = fitting.mo_filter("FeatureState", "CXC4020037")
    
    #FeatureState
    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState']
    eulmc['CXC4020037'] = eulmc['Siteid'].map(dict_fitur)

    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState_recomend']
    eulmc['CXC4020037_recomend'] = eulmc['Siteid'].map(dict_fitur)
    
    #NodeBLocalCell
    dict_fitur = NodeBLocalCell.set_index('CellId').to_dict()['featCtrlEulMc']
    eulmc['featCtrlEulMc'] = eulmc['Cellid'].map(dict_fitur)

    dict_fitur = NodeBLocalCell.set_index('CellId').to_dict()['featCtrlEulMc_recomend']
    eulmc['featCtrlEulMc_recomend'] = eulmc['Cellid'].map(dict_fitur)
    
    #RbsLocalCell
    dict_fitur = RbsLocalCell.set_index('CellId').to_dict()['featureStateEulMc']
    eulmc['featureStateEulMc'] = eulmc['Cellid'].map(dict_fitur)

    dict_fitur = RbsLocalCell.set_index('CellId').to_dict()['featureStateEulMc_recomend']
    eulmc['featureStateEulMc_recomend'] = eulmc['Cellid'].map(dict_fitur)
    
    eulmc = fitting.state(eulmc, "featCtrlEulMc", "featureStateEulMc")
    eulmc = fitting.check(eulmc)
    return eulmc

def hsdpamc():
    hsdpamc = fitting.fitting_template_3G()
    NodeBLocalCell = fitting.mo_read("NodeBLocalCell")
    RbsLocalCell = fitting.mo_read("RbsLocalCell")
    fitur = fitting.mo_filter("FeatureState", "CXC4020029")
    
    #FeatureState
    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState']
    hsdpamc['CXC4020029'] = hsdpamc['Siteid'].map(dict_fitur)

    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState_recomend']
    hsdpamc['CXC4020029_recomend'] = hsdpamc['Siteid'].map(dict_fitur)
    
    #NodeBLocalCell
    dict_fitur = NodeBLocalCell.set_index('CellId').to_dict()['featCtrlHsdpaMc']
    hsdpamc['featCtrlHsdpaMc'] = hsdpamc['Cellid'].map(dict_fitur)

    dict_fitur = NodeBLocalCell.set_index('CellId').to_dict()['featCtrlHsdpaMc_recomend']
    hsdpamc['featCtrlHsdpaMc_recomend'] = hsdpamc['Cellid'].map(dict_fitur)
    
    #RbsLocalCell
    dict_fitur = RbsLocalCell.set_index('CellId').to_dict()['featureStateHsdpaMc']
    hsdpamc['featureStateHsdpaMc'] = hsdpamc['Cellid'].map(dict_fitur)

    dict_fitur = RbsLocalCell.set_index('CellId').to_dict()['featureStateHsdpaMc_recomend']
    hsdpamc['featureStateHsdpaMc_recomend'] = hsdpamc['Cellid'].map(dict_fitur)
    
    hsdpamc = fitting.state(hsdpamc, "featCtrlHsdpaMc", "featureStateHsdpaMc")
    hsdpamc = fitting.check(hsdpamc)
    return hsdpamc

def ce_efficiency():
    ce_efficiency = fitting.fitting_template_3G()
    NodeBFunction = fitting.mo_read("NodeBFunction")
    fitur1 = fitting.mo_filter("FeatureState", "CXC4020103")
    fitur2 = fitting.mo_filter("FeatureState", "CXC4021014")
    fitur3 = fitting.mo_filter("FeatureState", "CXC4020075")
    fitur4 = fitting.mo_filter("FeatureState", "CXC4020069")
    
    #FeatureState
    dict_fitur = fitur1.set_index('SiteID').to_dict()['featureState']
    ce_efficiency['CXC4020103'] = ce_efficiency['Siteid'].map(dict_fitur)

    dict_fitur = fitur1.set_index('SiteID').to_dict()['featureState_recomend']
    ce_efficiency['CXC4020103_recomend'] = ce_efficiency['Siteid'].map(dict_fitur)
    
    dict_fitur = fitur2.set_index('SiteID').to_dict()['featureState']
    ce_efficiency['CXC4021014'] = ce_efficiency['Siteid'].map(dict_fitur)

    dict_fitur = fitur2.set_index('SiteID').to_dict()['featureState_recomend']
    ce_efficiency['CXC4021014_recomend'] = ce_efficiency['Siteid'].map(dict_fitur)
    
    dict_fitur = fitur3.set_index('SiteID').to_dict()['featureState']
    ce_efficiency['CXC4020075'] = ce_efficiency['Siteid'].map(dict_fitur)

    dict_fitur = fitur3.set_index('SiteID').to_dict()['featureState_recomend']
    ce_efficiency['CXC4020075_recomend'] = ce_efficiency['Siteid'].map(dict_fitur)
    
    dict_fitur = fitur4.set_index('SiteID').to_dict()['featureState']
    ce_efficiency['CXC4020069'] = ce_efficiency['Siteid'].map(dict_fitur)

    dict_fitur = fitur4.set_index('SiteID').to_dict()['featureState_recomend']
    ce_efficiency['CXC4020069_recomend'] = ce_efficiency['Siteid'].map(dict_fitur)
    
    #NodeBFunction
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateAdditionalCeExtForEul']
    ce_efficiency['featureStateAdditionalCeExtForEul'] = ce_efficiency['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateAdditionalCeExtForEul_recomend']
    ce_efficiency['featureStateAdditionalCeExtForEul_recomend'] = ce_efficiency['Siteid'].map(dict_fitur)
    
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateCeCapEul']
    ce_efficiency['featureStateCeCapEul'] = ce_efficiency['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateCeCapEul_recomend']
    ce_efficiency['featureStateCeCapEul_recomend'] = ce_efficiency['Siteid'].map(dict_fitur)
    
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateCeEfficiencyEul']
    ce_efficiency['featureStateCeEfficiencyEul'] = ce_efficiency['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateCeEfficiencyEul_recomend']
    ce_efficiency['featureStateCeEfficiencyEul_recomend'] = ce_efficiency['Siteid'].map(dict_fitur)
    
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateCeExtForEul']
    ce_efficiency['featureStateCeExtForEul'] = ce_efficiency['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateCeExtForEul_recomend']
    ce_efficiency['featureStateCeExtForEul_recomend'] = ce_efficiency['Siteid'].map(dict_fitur)
    
    ce_efficiency = fitting.state(ce_efficiency, "CXC4020103", "featureStateAdditionalCeExtForEul")
    ce_efficiency = fitting.state(ce_efficiency, "CXC4021014", "featureStateCeCapEul")
    ce_efficiency = fitting.state(ce_efficiency, "CXC4020075", "featureStateCeEfficiencyEul")
    ce_efficiency = fitting.state(ce_efficiency, "CXC4020069", "featureStateCeExtForEul")
    ce_efficiency = fitting.check(ce_efficiency)
    return ce_efficiency

def ul_interference():
    ul_interference = fitting.fitting_template_3G()
    NodeBFunction = fitting.mo_read("NodeBFunction")
    fitur1 = fitting.mo_filter("FeatureState", "CXC4020065")
    fitur2 = fitting.mo_filter("FeatureState", "CXC4020070")
    fitur3 = fitting.mo_filter("FeatureState", "CXC4020061")
    
    #FeatureState
    dict_fitur = fitur1.set_index('SiteID').to_dict()['featureState']
    ul_interference['CXC4020065'] = ul_interference['Siteid'].map(dict_fitur)

    dict_fitur = fitur1.set_index('SiteID').to_dict()['featureState_recomend']
    ul_interference['CXC4020065_recomend'] = ul_interference['Siteid'].map(dict_fitur)
    
    dict_fitur = fitur2.set_index('SiteID').to_dict()['featureState']
    ul_interference['CXC4020070'] = ul_interference['Siteid'].map(dict_fitur)

    dict_fitur = fitur2.set_index('SiteID').to_dict()['featureState_recomend']
    ul_interference['CXC4020070_recomend'] = ul_interference['Siteid'].map(dict_fitur)
    
    dict_fitur = fitur3.set_index('SiteID').to_dict()['featureState']
    ul_interference['CXC4020061'] = ul_interference['Siteid'].map(dict_fitur)

    dict_fitur = fitur3.set_index('SiteID').to_dict()['featureState_recomend']
    ul_interference['CXC4020061_recomend'] = ul_interference['Siteid'].map(dict_fitur)
    
    #NodeBFunction
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateInterferenceSuppression']
    ul_interference['featureStateInterferenceSuppression'] = ul_interference['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateInterferenceSuppression_recomend']
    ul_interference['featureStateInterferenceSuppression_recomend'] = ul_interference['Siteid'].map(dict_fitur)
    
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateIntSuppEul10ms']
    ul_interference['featureStateIntSuppEul10ms'] = ul_interference['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateIntSuppEul10ms_recomend']
    ul_interference['featureStateIntSuppEul10ms_recomend'] = ul_interference['Siteid'].map(dict_fitur)
    
    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateIntSuppAllBearers']
    ul_interference['featureStateIntSuppAllBearers'] = ul_interference['Siteid'].map(dict_fitur)

    dict_fitur = NodeBFunction.set_index('SiteID').to_dict()['featureStateIntSuppAllBearers_recomend']
    ul_interference['featureStateIntSuppAllBearers_recomend'] = ul_interference['Siteid'].map(dict_fitur)
    
    ul_interference = fitting.state(ul_interference, "CXC4020065", "featureStateInterferenceSuppression")
    ul_interference = fitting.state(ul_interference, "CXC4020070", "featureStateIntSuppEul10ms")
    ul_interference = fitting.state(ul_interference, "CXC4020061", "featureStateIntSuppAllBearers")
    ul_interference = fitting.check(ul_interference)
    return ul_interference

def u2l():
    u2l = fitting.fitting_template_3G()
    EUtranFreqRelation1 = fitting.mo_filter("EUtranFreqRelation", 275)
    EUtranFreqRelation2 = fitting.mo_filter("EUtranFreqRelation", 1850)
    EUtranFreqRelation3 = fitting.mo_filter("EUtranFreqRelation", 3500)
    EUtranFreqRelation4 = fitting.mo_filter("EUtranFreqRelation", 38750)
    EUtranFreqRelation5 = fitting.mo_filter("EUtranFreqRelation", 38894)
    
    #EutranFreqRelation
    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['qRxLevMin']
    u2l['275qRxLevMin'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['qRxLevMin_recomend']
    u2l['275qRxLevMin_recomend'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['threshXHigh']
    u2l['275threshHigh'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['threshXHigh_recomend']
    u2l['275threshHigh_recomend'] = u2l['Cellid'].map(dict_fitur)
    
    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['qRxLevMin']
    u2l['1850qRxLevMin'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['qRxLevMin_recomend']
    u2l['1850qRxLevMin_recomend'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['threshXHigh']
    u2l['1850threshHigh'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['threshXHigh_recomend']
    u2l['1850threshHigh_recomend'] = u2l['Cellid'].map(dict_fitur)
    
    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['qRxLevMin']
    u2l['3500qRxLevMin'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['qRxLevMin_recomend']
    u2l['3500qRxLevMin_recomend'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['threshXHigh']
    u2l['3500threshHigh'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['threshXHigh_recomend']
    u2l['3500threshHigh_recomend'] = u2l['Cellid'].map(dict_fitur)
    
    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['qRxLevMin']
    u2l['38750qRxLevMin'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['qRxLevMin_recomend']
    u2l['38750qRxLevMin_recomend'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['threshXHigh']
    u2l['38750threshHigh'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['threshXHigh_recomend']
    u2l['38750threshHigh_recomend'] = u2l['Cellid'].map(dict_fitur)
    
    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['qRxLevMin']
    u2l['38894qRxLevMin'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['qRxLevMin_recomend']
    u2l['38894qRxLevMin_recomend'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['threshXHigh']
    u2l['38894threshHigh'] = u2l['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['threshXHigh_recomend']
    u2l['38894threshHigh_recomend'] = u2l['Cellid'].map(dict_fitur)
    
    u2l = fitting.check(u2l)
    return u2l
