import fitting
import pandas as pd
import numpy as np
import sys
import os
import traceback
import numbers
import re
from datetime import datetime
import warnings
warnings.filterwarnings("ignore")

def HS_Thp_Improvement(template):
    HS_Thp_Improvement = fitting.fitting_template_3G()

    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "Hsdsch", "qam64Support", template)
    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "MultiCarrier", "multiCarrierSupport", template)
    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "RbsLocalCell", "featureStateHsAdaptiveBler", template)
    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "NodeBLocalCell", "featCtrlHsAdaptiveBler", template)
    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "Hsdsch", "cqiFeedbackCycle", template)    
    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "Hsdsch", "initialCqiRepetitionFactor", template)
    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "RbsLocalCell", "maxNumHsPdschCodes", template)
    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "RbsLocalCell", "hsPowerMargin", template)
    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "Hsdsch", "codeThresholdPdu656", template)
    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "Hsdsch", "numHsPdschCodes", template)
    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "Hsdsch", "numHsScchCodes", template)
    HS_Thp_Improvement = fitting.type1(HS_Thp_Improvement, "Hsdsch", "hsMeasurementPowerOffset", template)  	
       		        
    HS_Thp_Improvement = fitting.no_recomend(HS_Thp_Improvement)
    HS_Thp_Improvement = HS_Thp_Improvement.fillna("#NULL")
    return HS_Thp_Improvement

def EUL_Thp_Improvement(template):
    EUL_Thp_Improvement = fitting.fitting_template_3G()

    EUL_Thp_Improvement = fitting.type1(EUL_Thp_Improvement, "Eul", "edchTti2Support", template)
    EUL_Thp_Improvement = fitting.type1(EUL_Thp_Improvement, "MultiCarrier", "eulMultiCarrierSupport", template)
    EUL_Thp_Improvement = fitting.type1(EUL_Thp_Improvement, "Eul", "pathlossThresholdEulTti2", template)
    EUL_Thp_Improvement = fitting.type1(EUL_Thp_Improvement, "UtranCell", "eulServingCellUsersAdm", template)
    EUL_Thp_Improvement = fitting.type1(EUL_Thp_Improvement, "UtranCell", "eulServingCellUsersAdmTti2", template)    
    
    EUL_Thp_Improvement = fitting.no_recomend(EUL_Thp_Improvement)
    EUL_Thp_Improvement = EUL_Thp_Improvement.fillna("#NULL")
    return EUL_Thp_Improvement
    
def Access_and_retain(template):
    Access_and_retain = fitting.fitting_template_3G()

    Access_and_retain = fitting.type1(Access_and_retain, "RbsLocalCell", "maxNumHsdpaUsers", template)
    Access_and_retain = fitting.type1(Access_and_retain, "UtranCell", "hsdpaUsersAdm", template)
    Access_and_retain = fitting.type1(Access_and_retain, "RbsLocalCell", "maxNumEulUsers", template)
    Access_and_retain = fitting.type1(Access_and_retain, "UtranCell", "eulServingCellUsersAdm", template)
    Access_and_retain = fitting.type1(Access_and_retain, "UtranCell", "hsdpaUsersOffloadLimit", template)
    Access_and_retain = fitting.type1(Access_and_retain, "NodeBFunction", "featureStateAdditionalCeExtForEul", template) 
    Access_and_retain = fitting.type1(Access_and_retain, "NodeBFunction", "featureStateCeCapEul", template) 
    Access_and_retain = fitting.type1(Access_and_retain, "NodeBFunction", "featureStateCeEfficiencyEul", template) 
    Access_and_retain = fitting.type1(Access_and_retain, "NodeBFunction", "featureStateCeExtForEul", template)  
 
    Access_and_retain = fitting.no_recomend(Access_and_retain)
    Access_and_retain = Access_and_retain.fillna("#NULL")
    return Access_and_retain   

def RwR_to_LTE(template):
    RwR_to_LTE = fitting.fitting_template_3G()

    RwR_to_LTE = fitting.type1(RwR_to_LTE, "UtranCell", "releaseRedirect", template)
    RwR_to_LTE = fitting.type1(RwR_to_LTE, "UtranCell", "releaseRedirectEutraTriggers_csFallbackCsRelease", template)
    RwR_to_LTE = fitting.type1(RwR_to_LTE, "UtranCell", "releaseRedirectEutraTriggers_csFallbackDchToFach", template)
    RwR_to_LTE = fitting.type1(RwR_to_LTE, "UtranCell", "releaseRedirectEutraTriggers_dchToFach", template)
    RwR_to_LTE = fitting.type1(RwR_to_LTE, "UtranCell", "releaseRedirectEutraTriggers_fachToUra", template)
    RwR_to_LTE = fitting.type1(RwR_to_LTE, "UtranCell", "releaseRedirectEutraTriggers_fastDormancy", template)
    RwR_to_LTE = fitting.type1(RwR_to_LTE, "UtranCell", "releaseRedirectEutraTriggers_normalRelease", template)

    RwR_to_LTE = fitting.no_recomend(RwR_to_LTE)
    RwR_to_LTE = RwR_to_LTE.fillna("#NULL")
    return RwR_to_LTE


'''
def Hsdsch(template):
    Hsdsch = fitting.fitting_template_3G()
    Hsdsch = fitting.type1(Hsdsch, "Hsdsch", template)
    Hsdsch = fitting.no_recomend(Hsdsch)
    Hsdsch = Hsdsch.fillna("#NULL")
    return Hsdsch

def MultiCarrier(template):
    MultiCarrier = fitting.fitting_template_3G()
    MultiCarrier = fitting.type1(MultiCarrier, "MultiCarrier", template)
    MultiCarrier = fitting.no_recomend(MultiCarrier)
    MultiCarrier = MultiCarrier.fillna("#NULL")
    return MultiCarrier

def RbsLocalCell(template):
    RbsLocalCell = fitting.fitting_template_3G()
    RbsLocalCell = fitting.type1(RbsLocalCell, "RbsLocalCell", template)
    RbsLocalCell = fitting.no_recomend(RbsLocalCell)
    RbsLocalCell = RbsLocalCell.fillna("#NULL")
    return RbsLocalCell

def Eul(template):
    Eul = fitting.fitting_template_3G()
    Eul = fitting.type1(Eul, "Eul", template)
    Eul = fitting.no_recomend(Eul)
    Eul = Eul.fillna("#NULL")
    return Eul

def UtranCell(template):
    UtranCell = fitting.fitting_template_3G()
    UtranCell = fitting.type1(UtranCell, "UtranCell", template)
    UtranCell = fitting.no_recomend(UtranCell)
    UtranCell = UtranCell.fillna("#NULL")
    return UtranCell

def NodeBFunction(template):
    NodeBFunction = fitting.fitting_template_3G()
    NodeBFunction = fitting.type1(NodeBFunction, "NodeBFunction", template)
    NodeBFunction = fitting.no_recomend(NodeBFunction)
    NodeBFunction = NodeBFunction.fillna("#NULL")
    return NodeBFunction
'''
