import fitting

# Param 4G
def Uplink_schedulink():
    Uplink_schedulink = fitting.fitting_template()
    fitur = fitting.mo_filter("FeatureState", "CXC4012129")
    optional = fitting.mo_filter("OptionalFeatureLicense", "DataAwareUplinkScheduling")
    EUtranCellFDD = fitting.mo_read("EUtranCellFDD")
    EUtranCellTDD = fitting.mo_read("EUtranCellTDD")

    #feature state
    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState']
    Uplink_schedulink['CXC4012129'] = Uplink_schedulink['Siteid'].map(dict_fitur)

    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState_recomend']
    Uplink_schedulink['CXC4012129_recomend'] = Uplink_schedulink['Siteid'].map(dict_fitur)

    #optionalFeature
    dict_fitur = optional.set_index('SiteID').to_dict()['featureState']
    Uplink_schedulink['DataAwareUplinkScheduling'] = Uplink_schedulink['Siteid'].map(dict_fitur)

    dict_fitur = optional.set_index('SiteID').to_dict()['featureState_recomend']
    Uplink_schedulink['DataAwareUplinkScheduling_recomend'] = Uplink_schedulink['Siteid'].map(dict_fitur)

    #EUtranCellFDD
    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['bsrThreshold']
    Uplink_schedulink['bsrThreshold'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['bsrThreshold_recomend']
    Uplink_schedulink['bsrThreshold_recomend'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['noOfUlImprovedUe']
    Uplink_schedulink['noOfUlImprovedUe'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['noOfUlImprovedUe_recomend']
    Uplink_schedulink['noOfUlImprovedUe_recomend'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['ulImprovedUeSchedLastEnabled']
    Uplink_schedulink['ulImprovedUeSchedLastEnabled'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['ulImprovedUeSchedLastEnabled_recomend']
    Uplink_schedulink['ulImprovedUeSchedLastEnabled_recomend'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    #EUtranCellTDD
    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['bsrThreshold']
    Uplink_schedulink['tddbsrThreshold'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['bsrThreshold_recomend']
    Uplink_schedulink['tddbsrThreshold_recomend'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['noOfUlImprovedUe']
    Uplink_schedulink['tddnoOfUlImprovedUe'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['noOfUlImprovedUe_recomend']
    Uplink_schedulink['tddnoOfUlImprovedUe_recomend'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['ulImprovedUeSchedLastEnabled']
    Uplink_schedulink['tddulImprovedUeSchedLastEnabled'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['ulImprovedUeSchedLastEnabled_recomend']
    Uplink_schedulink['tddulImprovedUeSchedLastEnabled_recomend'] = Uplink_schedulink['Cellid'].map(dict_fitur)

    # Merge feature&OptionalFeature EUtranCellFDD&EUtranCellTDD
    Uplink_schedulink = fitting.state(Uplink_schedulink, "CXC4012129", "DataAwareUplinkScheduling")
    Uplink_schedulink = fitting.eutran(Uplink_schedulink, "bsrThreshold")
    Uplink_schedulink = fitting.eutran(Uplink_schedulink, "noOfUlImprovedUe")
    Uplink_schedulink = fitting.eutran(Uplink_schedulink, "ulImprovedUeSchedLastEnabled")

    Uplink_schedulink = fitting.check(Uplink_schedulink)
    return Uplink_schedulink

def qam64_ul():
    qam64_ul = fitting.fitting_template()
    fitur = fitting.mo_filter("FeatureState", "CXC4011946")
    optional = fitting.mo_filter("OptionalFeatureLicense", "Ul64Qam")
    EUtranCellFDD = fitting.mo_read("EUtranCellFDD")
    EUtranCellTDD = fitting.mo_read("EUtranCellTDD")

    #feature state
    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState']
    qam64_ul['CXC4011946'] = qam64_ul['Siteid'].map(dict_fitur)

    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState_recomend']
    qam64_ul['CXC4011946_recomend'] = qam64_ul['Siteid'].map(dict_fitur)

    #optionalFeature
    dict_fitur = optional.set_index('SiteID').to_dict()['featureState']
    qam64_ul['Ul64Qam'] = qam64_ul['Siteid'].map(dict_fitur)

    dict_fitur = optional.set_index('SiteID').to_dict()['featureState_recomend']
    qam64_ul['Ul64Qam_recomend'] = qam64_ul['Siteid'].map(dict_fitur)

    #EUtranCellFDD
    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['ul64qamEnabled']
    qam64_ul['ul64qamEnabled'] = qam64_ul['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['ul64qamEnabled_recomend']
    qam64_ul['ul64qamEnabled_recomend'] = qam64_ul['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['puschPwrOffset64qam']
    qam64_ul['puschPwrOffset64qam'] = qam64_ul['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['puschPwrOffset64qam_recomend']
    qam64_ul['puschPwrOffset64qam_recomend'] = qam64_ul['Cellid'].map(dict_fitur)

    #EUtranCellTDD
    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['ul64qamEnabled']
    qam64_ul['tddul64qamEnabled'] = qam64_ul['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['ul64qamEnabled_recomend']
    qam64_ul['tddul64qamEnabled_recomend'] = qam64_ul['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['puschPwrOffset64qam']
    qam64_ul['tddpuschPwrOffset64qam'] = qam64_ul['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['puschPwrOffset64qam_recomend']
    qam64_ul['tddpuschPwrOffset64qam_recomend'] = qam64_ul['Cellid'].map(dict_fitur)

    # Merge feature&OptionalFeature EUtranCellFDD&EUtranCellTDD
    qam64_ul = fitting.state(qam64_ul, "CXC4011946", "Ul64Qam")
    qam64_ul = fitting.eutran(qam64_ul, "ul64qamEnabled")
    qam64_ul = fitting.eutran(qam64_ul, "puschPwrOffset64qam")

    qam64_ul = fitting.check(qam64_ul)
    return qam64_ul

def cell_range():
    cell_range = fitting.fitting_template()
    fitur = fitting.mo_filter("FeatureState", "CXC4010967")
    optional = fitting.mo_filter("OptionalFeatureLicense", "MaximumCellRange")
    EUtranCellFDD = fitting.mo_read("EUtranCellFDD")
    EUtranCellTDD = fitting.mo_read("EUtranCellTDD")

    #feature state
    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState']
    cell_range['CXC4010967'] = cell_range['Siteid'].map(dict_fitur)

    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState_recomend']
    cell_range['CXC4010967_recomend'] = cell_range['Siteid'].map(dict_fitur)

    #optionalFeature
    dict_fitur = optional.set_index('SiteID').to_dict()['featureState']
    cell_range['MaximumCellRange'] = cell_range['Siteid'].map(dict_fitur)

    dict_fitur = optional.set_index('SiteID').to_dict()['featureState_recomend']
    cell_range['MaximumCellRange_recomend'] = cell_range['Siteid'].map(dict_fitur)

    #EUtranCellFDD
    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['cellRange']
    cell_range['cellRange'] = cell_range['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['cellRange_recomend']
    cell_range['cellRange_recomend'] = cell_range['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['crsGain']
    cell_range['crsGain'] = cell_range['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['crsGain_recomend']
    cell_range['crsGain_recomend'] = cell_range['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['pdschTypeBGain']
    cell_range['pdschTypeBGain'] = cell_range['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['pdschTypeBGain_recomend']
    cell_range['pdschTypeBGain_recomend'] = cell_range['Cellid'].map(dict_fitur)

    #EUtranCellTDD
    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['cellRange']
    cell_range['tddcellRange'] = cell_range['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['cellRange_recomend']
    cell_range['tddcellRange_recomend'] = cell_range['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['crsGain']
    cell_range['tddcrsGain'] = cell_range['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['crsGain_recomend']
    cell_range['tddcrsGain_recomend'] = cell_range['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['pdschTypeBGain']
    cell_range['tddpdschTypeBGain'] = cell_range['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['pdschTypeBGain_recomend']
    cell_range['tddpdschTypeBGain_recomend'] = cell_range['Cellid'].map(dict_fitur)

    # Merge feature&OptionalFeature EUtranCellFDD&EUtranCellTDD
    cell_range = fitting.state(cell_range, "CXC4010967", "MaximumCellRange")
    cell_range = fitting.eutran(cell_range, "cellRange")
    cell_range = fitting.eutran(cell_range, "crsGain")
    cell_range = fitting.eutran(cell_range, "pdschTypeBGain")

    cell_range = fitting.maxRange(cell_range)

    cell_range = fitting.check(cell_range)
    return cell_range

def irow():
    irow = fitting.fitting_template()
    fitur = fitting.mo_filter("FeatureState", "CXC4011478")
    optional = fitting.mo_filter("OptionalFeatureLicense", "InterRatOffloadToUtran")

    #feature state
    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState']
    irow['CXC4011478'] = irow['Siteid'].map(dict_fitur)

    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState_recomend']
    irow['CXC4011478_recomend'] = irow['Siteid'].map(dict_fitur)

    #optionalFeature
    dict_fitur = optional.set_index('SiteID').to_dict()['featureState']
    irow['InterRatOffloadToUtran'] = irow['Siteid'].map(dict_fitur)

    dict_fitur = optional.set_index('SiteID').to_dict()['featureState_recomend']
    irow['InterRatOffloadToUtran_recomend'] = irow['Siteid'].map(dict_fitur)

    # Merge feature&OptionalFeature EUtranCellFDD&EUtranCellTDD
    irow = fitting.state(irow, "CXC4011478", "InterRatOffloadToUtran")

    irow = fitting.check(irow)
    return irow

def ul_comp():
    ul_comp = fitting.fitting_template()
    fitur = fitting.mo_filter("FeatureState", "CXC4011444")
    optional = fitting.mo_filter("OptionalFeatureLicense", "operationalState")
    UlCompGroup = fitting.mo_read("UlCompGroup")

    #feature state
    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState']
    ul_comp['CXC4011444'] = ul_comp['Siteid'].map(dict_fitur)

    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState_recomend']
    ul_comp['CXC4011444_recomend'] = ul_comp['Siteid'].map(dict_fitur)

    #optionalFeature
    dict_fitur = optional.set_index('SiteID').to_dict()['featureState']
    ul_comp['operationalState'] = ul_comp['Siteid'].map(dict_fitur)

    dict_fitur = optional.set_index('SiteID').to_dict()['featureState_recomend']
    ul_comp['operationalState_recomend'] = ul_comp['Siteid'].map(dict_fitur)
    
    #ULCompgroup
    dict_fitur = UlCompGroup.set_index('SiteID').to_dict()['operationalState']
    ul_comp['operationalState_UlCompGroup'] = ul_comp['Siteid'].map(dict_fitur)

    dict_fitur = UlCompGroup.set_index('SiteID').to_dict()['operationalState_recomend']
    ul_comp['operationalState_UlCompGroup_recomend'] = ul_comp['Siteid'].map(dict_fitur)

    # Merge feature&OptionalFeature EUtranCellFDD&EUtranCellTDD
    ul_comp = fitting.state(ul_comp, "CXC4011444", "operationalState")

    ul_comp = fitting.check(ul_comp)
    return ul_comp

def ca():
    ca = fitting.fitting_template()
    fitur1 = fitting.mo_filter("FeatureState", "CXC4011476")
    fitur2 = fitting.mo_filter("FeatureState", "CXC4011559")
    fitur3 = fitting.mo_filter("FeatureState", "CXC4012097")
    fitur4 = fitting.mo_filter("FeatureState", "CXC4011714")
    fitur5 = fitting.mo_filter("FeatureState", "CXC4011922")
    optional1 = fitting.mo_filter("OptionalFeatureLicense", "CarrierAggregation")
    optional2 = fitting.mo_filter("OptionalFeatureLicense", "DynamicScellSelection")
    optional3 = fitting.mo_filter("OptionalFeatureLicense", "ConfigurableScellPrio")
    optional4 = fitting.mo_filter("OptionalFeatureLicense", "ThreeDlCarrierAggregation")
    optional5 = fitting.mo_filter("OptionalFeatureLicense", "CarrierAggregationFddTdd")
    CarrierAggregationFunction = fitting.mo_read("CarrierAggregationFunction")

    #feature state
    dict_fitur = fitur1.set_index('SiteID').to_dict()['featureState']
    ca['CXC4011476'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = fitur1.set_index('SiteID').to_dict()['featureState_recomend']
    ca['CXC4011476_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = fitur2.set_index('SiteID').to_dict()['featureState']
    ca['CXC4011559'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = fitur2.set_index('SiteID').to_dict()['featureState_recomend']
    ca['CXC4011559_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = fitur3.set_index('SiteID').to_dict()['featureState']
    ca['CXC4012097'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = fitur3.set_index('SiteID').to_dict()['featureState_recomend']
    ca['CXC4012097_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = fitur4.set_index('SiteID').to_dict()['featureState']
    ca['CXC4011714'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = fitur4.set_index('SiteID').to_dict()['featureState_recomend']
    ca['CXC4011714_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = fitur5.set_index('SiteID').to_dict()['featureState']
    ca['CXC4011922'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = fitur5.set_index('SiteID').to_dict()['featureState_recomend']
    ca['CXC4011922_recomend'] = ca['Siteid'].map(dict_fitur)

    #optionalFeature
    dict_fitur = optional1.set_index('SiteID').to_dict()['featureState']
    ca['CarrierAggregation'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = optional1.set_index('SiteID').to_dict()['featureState_recomend']
    ca['CarrierAggregation_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = optional2.set_index('SiteID').to_dict()['featureState']
    ca['DynamicScellSelection'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = optional2.set_index('SiteID').to_dict()['featureState_recomend']
    ca['DynamicScellSelection_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = optional3.set_index('SiteID').to_dict()['featureState']
    ca['ConfigurableScellPrio'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = optional3.set_index('SiteID').to_dict()['featureState_recomend']
    ca['ConfigurableScellPrio_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = optional4.set_index('SiteID').to_dict()['featureState']
    ca['ThreeDlCarrierAggregation'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = optional4.set_index('SiteID').to_dict()['featureState_recomend']
    ca['ThreeDlCarrierAggregation_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = optional5.set_index('SiteID').to_dict()['featureState']
    ca['CarrierAggregationFddTdd'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = optional5.set_index('SiteID').to_dict()['featureState_recomend']
    ca['CarrierAggregationFddTdd_recomend'] = ca['Siteid'].map(dict_fitur)

    #CarrierAggregationFunction
    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['caUsageLimit']
    ca['caUsageLimit'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['caUsageLimit_recomend']
    ca['caUsageLimit_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['dynamicSCellSelectionMethod']
    ca['dynamicSCellSelectionMethod'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['dynamicSCellSelectionMethod_recomend']
    ca['dynamicSCellSelectionMethod_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['laaSCellActProhibitTimer']
    ca['laaSCellActProhibitTimer'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['laaSCellActProhibitTimer_recomend']
    ca['laaSCellActProhibitTimer_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['laaSCellDeactProhibitTimer']
    ca['laaSCellDeactProhibitTimer'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['laaSCellDeactProhibitTimer_recomend']
    ca['laaSCellDeactProhibitTimer_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['sCellActDeactDataThres']
    ca['sCellActDeactDataThres'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['sCellActDeactDataThres_recomend']
    ca['sCellActDeactDataThres_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['sCellActDeactDataThresHyst']
    ca['sCellActDeactDataThresHyst'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['sCellActDeactDataThresHyst_recomend']
    ca['sCellActDeactDataThresHyst_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['sCellActProhibitTimer']
    ca['sCellActProhibitTimer'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['sCellActProhibitTimer_recomend']
    ca['sCellActProhibitTimer_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['sCellScheduleSinrThres']
    ca['sCellScheduleSinrThres'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['sCellScheduleSinrThres_recomend']
    ca['sCellScheduleSinrThres_recomend'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['sCellSelectionMode']
    ca['sCellSelectionMode'] = ca['Siteid'].map(dict_fitur)

    dict_fitur = CarrierAggregationFunction.set_index('SiteID').to_dict()['sCellSelectionMode_recomend']
    ca['sCellSelectionMode_recomend'] = ca['Siteid'].map(dict_fitur)

    # Merge feature&OptionalFeature EUtranCellFDD&EUtranCellTDD
    ca = fitting.state(ca, "CXC4011476", "CarrierAggregation")
    ca = fitting.state(ca, "CXC4011559", "DynamicScellSelection")
    ca = fitting.state(ca, "CXC4012097", "ConfigurableScellPrio")
    ca = fitting.state(ca, "CXC4011714", "ThreeDlCarrierAggregation")
    ca = fitting.state(ca, "CXC4011922", "CarrierAggregationFddTdd")

    ca = fitting.check(ca)
    return ca

def rejection():
    rejection = fitting.fitting_template()
    fitur = fitting.mo_filter("FeatureState", "CXC4010319")
    optional = fitting.mo_filter("OptionalFeatureLicense", "Irc")

    #feature state
    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState']
    rejection['CXC4010319'] = rejection['Siteid'].map(dict_fitur)

    dict_fitur = fitur.set_index('SiteID').to_dict()['featureState_recomend']
    rejection['CXC4010319_recomend'] = rejection['Siteid'].map(dict_fitur)

    #optionalFeature
    dict_fitur = optional.set_index('SiteID').to_dict()['featureState']
    rejection['Irc'] = rejection['Siteid'].map(dict_fitur)

    dict_fitur = optional.set_index('SiteID').to_dict()['featureState_recomend']
    rejection['Irc_recomend'] = rejection['Siteid'].map(dict_fitur)

    # Merge feature&OptionalFeature EUtranCellFDD&EUtranCellTDD
    rejection = fitting.state(rejection, "CXC4010319", "Irc")

    rejection = fitting.check(rejection)
    return rejection

def l2u():
    l2u = fitting.fitting_template()
    fitur1 = fitting.mo_filter("FeatureState", "CXC4011345")
    fitur2 = fitting.mo_filter("FeatureState", "CXC4011011")
    optional1 = fitting.mo_filter("OptionalFeatureLicense", "MobCtrlAtPoorCov")
    optional2 = fitting.mo_filter("OptionalFeatureLicense", "WcdmaHandover")
    EUtranCellFDD = fitting.mo_read("EUtranCellFDD")
    EUtranCellTDD = fitting.mo_read("EUtranCellTDD")
    UeMeasControl = fitting.mo_read("UeMeasControl")
    UtranFreqRelation = fitting.mo_read("UtranFreqRelation")
    ReportConfigSearch = fitting.mo_read("ReportConfigSearch")
    ReportConfigB2Utra = fitting.mo_read("ReportConfigB2Utra")
    EUtranFreqRelation1 = fitting.mo_filter("EUtranFreqRelation", 275)
    EUtranFreqRelation2 = fitting.mo_filter("EUtranFreqRelation", 1850)
    EUtranFreqRelation3 = fitting.mo_filter("EUtranFreqRelation", 3500)
    EUtranFreqRelation4 = fitting.mo_filter("EUtranFreqRelation", 38750)
    EUtranFreqRelation5 = fitting.mo_filter("EUtranFreqRelation", 38894)

    #feature state
    dict_fitur = fitur1.set_index('SiteID').to_dict()['featureState']
    l2u['CXC4011345'] = l2u['Siteid'].map(dict_fitur)

    dict_fitur = fitur1.set_index('SiteID').to_dict()['featureState_recomend']
    l2u['CXC4011345_recomend'] = l2u['Siteid'].map(dict_fitur)

    dict_fitur = fitur2.set_index('SiteID').to_dict()['featureState']
    l2u['CXC4011011'] = l2u['Siteid'].map(dict_fitur)

    dict_fitur = fitur2.set_index('SiteID').to_dict()['featureState_recomend']
    l2u['CXC4011011_recomend'] = l2u['Siteid'].map(dict_fitur)

    #optionalFeature
    dict_fitur = optional1.set_index('SiteID').to_dict()['featureState']
    l2u['MobCtrlAtPoorCov'] = l2u['Siteid'].map(dict_fitur)

    dict_fitur = optional1.set_index('SiteID').to_dict()['featureState_recomend']
    l2u['MobCtrlAtPoorCov_recomend'] = l2u['Siteid'].map(dict_fitur)

    dict_fitur = optional2.set_index('SiteID').to_dict()['featureState']
    l2u['WcdmaHandover'] = l2u['Siteid'].map(dict_fitur)

    dict_fitur = optional2.set_index('SiteID').to_dict()['featureState_recomend']
    l2u['WcdmaHandover_recomend'] = l2u['Siteid'].map(dict_fitur)

    #EUtranCellFDD
    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['covTriggerdBlindHoAllowed']
    l2u['covTriggerdBlindHoAllowed'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['covTriggerdBlindHoAllowed_recomend']
    l2u['covTriggerdBlindHoAllowed_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['mobCtrlAtPoorCovActive']
    l2u['mobCtrlAtPoorCovActive'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['mobCtrlAtPoorCovActive_recomend']
    l2u['mobCtrlAtPoorCovActive_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['qRxLevMin']
    l2u['qRxLevMin'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['qRxLevMin_recomend']
    l2u['qRxLevMin_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['sNonIntraSearch']
    l2u['sNonIntraSearch'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['sNonIntraSearch_recomend']
    l2u['sNonIntraSearch_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['threshServingLow']
    l2u['threshServingLow'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['threshServingLow_recomend']
    l2u['threshServingLow_recomend'] = l2u['Cellid'].map(dict_fitur)

    #EUtranCellTDD
    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['covTriggerdBlindHoAllowed']
    l2u['tddcovTriggerdBlindHoAllowed'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['covTriggerdBlindHoAllowed_recomend']
    l2u['tddcovTriggerdBlindHoAllowed_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['mobCtrlAtPoorCovActive']
    l2u['tddmobCtrlAtPoorCovActive'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['mobCtrlAtPoorCovActive_recomend']
    l2u['tddmobCtrlAtPoorCovActive_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['qRxLevMin']
    l2u['tddqRxLevMin'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['qRxLevMin_recomend']
    l2u['tddqRxLevMin_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['sNonIntraSearch']
    l2u['tddsNonIntraSearch'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['sNonIntraSearch_recomend']
    l2u['tddsNonIntraSearch_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['threshServingLow']
    l2u['tddthreshServingLow'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['threshServingLow_recomend']
    l2u['tddthreshServingLow_recomend'] = l2u['Cellid'].map(dict_fitur)

    #UeMeasControl
    dict_fitur = UeMeasControl.set_index('CellId').to_dict()['ueMeasurementsActiveIF']
    l2u['ueMeasurementsActiveIF'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = UeMeasControl.set_index('CellId').to_dict()['ueMeasurementsActiveIF_recomend']
    l2u['ueMeasurementsActiveIF_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = UeMeasControl.set_index('CellId').to_dict()['ueMeasurementsActive']
    l2u['ueMeasurementsActive'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = UeMeasControl.set_index('CellId').to_dict()['ueMeasurementsActive_recomend']
    l2u['ueMeasurementsActive_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = UeMeasControl.set_index('CellId').to_dict()['ueMeasurementsActiveUTRAN']
    l2u['ueMeasurementsActiveUTRAN'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = UeMeasControl.set_index('CellId').to_dict()['ueMeasurementsActiveUTRAN_recomend']
    l2u['ueMeasurementsActiveUTRAN_recomend'] = l2u['Cellid'].map(dict_fitur)

    #UtranFreqRelation
    dict_fitur = UtranFreqRelation.set_index('CellId').to_dict()['mobilityAction']
    l2u['mobilityAction'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = UtranFreqRelation.set_index('CellId').to_dict()['mobilityAction_recomend']
    l2u['mobilityAction_recomend'] = l2u['Cellid'].map(dict_fitur)

    #ReportConfigSearch
    dict_fitur = ReportConfigSearch.set_index('CellId').to_dict()['a1a2SearchThresholdRsrp']
    l2u['a1a2SearchThresholdRsrp'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigSearch.set_index('CellId').to_dict()['a1a2SearchThresholdRsrp_recomend']
    l2u['a1a2SearchThresholdRsrp_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigSearch.set_index('CellId').to_dict()['hysteresisA1A2SearchRsrp']
    l2u['hysteresisA1A2SearchRsrp'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigSearch.set_index('CellId').to_dict()['hysteresisA1A2SearchRsrp_recomend']
    l2u['hysteresisA1A2SearchRsrp_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigSearch.set_index('CellId').to_dict()['a2CriticalThresholdRsrp']
    l2u['a2CriticalThresholdRsrp'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigSearch.set_index('CellId').to_dict()['a2CriticalThresholdRsrp_recomend']
    l2u['a2CriticalThresholdRsrp_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigSearch.set_index('CellId').to_dict()['hysteresisA2CriticalRsrp']
    l2u['hysteresisA2CriticalRsrp'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigSearch.set_index('CellId').to_dict()['hysteresisA2CriticalRsrp_recomend']
    l2u['hysteresisA2CriticalRsrp_recomend'] = l2u['Cellid'].map(dict_fitur)

    #ReportConfigB2Utra
    dict_fitur = ReportConfigB2Utra.set_index('CellId').to_dict()['b2Threshold1Rsrp']
    l2u['b2Threshold1Rsrp'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigB2Utra.set_index('CellId').to_dict()['b2Threshold1Rsrp_recomend']
    l2u['b2Threshold1Rsrp_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigB2Utra.set_index('CellId').to_dict()['b2Threshold2RscpUtra']
    l2u['b2Threshold2RscpUtra'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigB2Utra.set_index('CellId').to_dict()['b2Threshold2RscpUtra_recomend']
    l2u['b2Threshold2RscpUtra_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigB2Utra.set_index('CellId').to_dict()['hysteresisB2']
    l2u['hysteresisB2'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigB2Utra.set_index('CellId').to_dict()['hysteresisB2_recomend']
    l2u['hysteresisB2_recomend'] = l2u['Cellid'].map(dict_fitur)

    #EUtranFreqRelation
    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['connectedModeMobilityPrio']
    l2u['275connectedModeMobilityPrio'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['connectedModeMobilityPrio_recomend']
    l2u['275connectedModeMobilityPrio_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['interFreqMeasType']
    l2u['275interFreqMeasType'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['interFreqMeasType_recomend']
    l2u['275interFreqMeasType_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['connectedModeMobilityPrio']
    l2u['1850connectedModeMobilityPrio'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['connectedModeMobilityPrio_recomend']
    l2u['1850connectedModeMobilityPrio_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['interFreqMeasType']
    l2u['1850interFreqMeasType'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['interFreqMeasType_recomend']
    l2u['1850interFreqMeasType_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['connectedModeMobilityPrio']
    l2u['3500connectedModeMobilityPrio'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['connectedModeMobilityPrio_recomend']
    l2u['3500connectedModeMobilityPrio_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['interFreqMeasType']
    l2u['3500interFreqMeasType'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['interFreqMeasType_recomend']
    l2u['3500interFreqMeasType_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['connectedModeMobilityPrio']
    l2u['38750connectedModeMobilityPrio'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['connectedModeMobilityPrio_recomend']
    l2u['38750connectedModeMobilityPrio_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['interFreqMeasType']
    l2u['38750interFreqMeasType'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['interFreqMeasType_recomend']
    l2u['38750interFreqMeasType_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['connectedModeMobilityPrio']
    l2u['38894connectedModeMobilityPrio'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['connectedModeMobilityPrio_recomend']
    l2u['38894connectedModeMobilityPrio_recomend'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['interFreqMeasType']
    l2u['38894interFreqMeasType'] = l2u['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['interFreqMeasType_recomend']
    l2u['38894interFreqMeasType_recomend'] = l2u['Cellid'].map(dict_fitur)

    # Merge feature&OptionalFeature EUtranCellFDD&EUtranCellTDD
    l2u = fitting.state(l2u, "CXC4011345", "MobCtrlAtPoorCov")
    l2u = fitting.state(l2u, "CXC4011011", "WcdmaHandover")
    l2u = fitting.eutran(l2u, "qRxLevMin")
    l2u = fitting.eutran(l2u, "sNonIntraSearch")
    l2u = fitting.eutran(l2u, "threshServingLow")
    l2u = fitting.eutran(l2u, "covTriggerdBlindHoAllowed")
    l2u = fitting.eutran(l2u, "mobCtrlAtPoorCovActive")

    l2u = fitting.check(l2u)
    return l2u

def iflb():
    iflb = fitting.fitting_template()
    fitur1 = fitting.mo_filter("FeatureState", "CXC4011663")
    fitur2 = fitting.mo_filter("FeatureState", "CXC4011664")
    fitur3 = fitting.mo_filter("FeatureState", "CXC4011554")
    fitur4 = fitting.mo_filter("FeatureState", "CXC4010770")
    fitur5 = fitting.mo_filter("FeatureState", "CXC4010974")
    fitur6 = fitting.mo_filter("FeatureState", "CXC4011443")
    fitur7 = fitting.mo_filter("FeatureState", "CXC4011319")
    fitur8 = fitting.mo_filter("FeatureState", "CXC4011373")

    optional1 = fitting.mo_filter("OptionalFeatureLicense", "PshoBasedCsfbToUtran")
    optional2 = fitting.mo_filter("OptionalFeatureLicense", "MeasBasedCsfbTargetSelection")
    optional3 = fitting.mo_filter("OptionalFeatureLicense", "IFLBActivationThreshold")
    optional4 = fitting.mo_filter("OptionalFeatureLicense", "InterFreqSessionContinuity")
    optional5 = fitting.mo_filter("OptionalFeatureLicense", "InterFrequencyLteHandover")
    optional6 = fitting.mo_filter("OptionalFeatureLicense", "IntraLTEInterModeHandover")
    optional7 = fitting.mo_filter("OptionalFeatureLicense", "InterFrequencyLoadBalancing")
    optional8 = fitting.mo_filter("OptionalFeatureLicense", "AutoCellCapEstFunction")

    UeMeasControl = fitting.mo_read("UeMeasControl")

    EUtranCellFDD = fitting.mo_read("EUtranCellFDD")
    EUtranCellTDD = fitting.mo_read("EUtranCellTDD")

    LoadBalancingFunction = fitting.mo_read("LoadBalancingFunction")

    ReportConfigA5 = fitting.mo_read("ReportConfigA5")

    ReportConfigEUtraInterFreqLb = fitting.mo_read("ReportConfigEUtraInterFreqLb")

    EUtranFreqRelation1 = fitting.mo_filter("EUtranFreqRelation", 275)
    EUtranFreqRelation2 = fitting.mo_filter("EUtranFreqRelation", 1850)
    EUtranFreqRelation3 = fitting.mo_filter("EUtranFreqRelation", 3500)
    EUtranFreqRelation4 = fitting.mo_filter("EUtranFreqRelation", 38750)
    EUtranFreqRelation5 = fitting.mo_filter("EUtranFreqRelation", 38894)

    #feature state
    dict_fitur = fitur1.set_index('SiteID').to_dict()['featureState']
    iflb['CXC4011663'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur1.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['CXC4011663_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur2.set_index('SiteID').to_dict()['featureState']
    iflb['CXC4011664'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur2.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['CXC4011664_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur3.set_index('SiteID').to_dict()['featureState']
    iflb['CXC4011554'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur3.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['CXC4011554_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur4.set_index('SiteID').to_dict()['featureState']
    iflb['CXC4010770'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur4.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['CXC4010770_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur5.set_index('SiteID').to_dict()['featureState']
    iflb['CXC4010974'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur5.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['CXC4010974_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur6.set_index('SiteID').to_dict()['featureState']
    iflb['CXC4011443'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur6.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['CXC4011443_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur7.set_index('SiteID').to_dict()['featureState']
    iflb['CXC4011319'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur7.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['CXC4011319_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur8.set_index('SiteID').to_dict()['featureState']
    iflb['CXC4011373'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = fitur8.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['CXC4011373_recomend'] = iflb['Siteid'].map(dict_fitur)

    #optionalFeature
    dict_fitur = optional1.set_index('SiteID').to_dict()['featureState']
    iflb['PshoBasedCsfbToUtran'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional1.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['PshoBasedCsfbToUtran_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional2.set_index('SiteID').to_dict()['featureState']
    iflb['MeasBasedCsfbTargetSelection'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional2.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['MeasBasedCsfbTargetSelection_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional3.set_index('SiteID').to_dict()['featureState']
    iflb['IFLBActivationThreshold'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional3.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['IFLBActivationThreshold_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional4.set_index('SiteID').to_dict()['featureState']
    iflb['InterFreqSessionContinuity'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional4.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['InterFreqSessionContinuity_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional5.set_index('SiteID').to_dict()['featureState']
    iflb['InterFrequencyLteHandover'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional5.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['InterFrequencyLteHandover_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional6.set_index('SiteID').to_dict()['featureState']
    iflb['IntraLTEInterModeHandover'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional6.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['IntraLTEInterModeHandover_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional7.set_index('SiteID').to_dict()['featureState']
    iflb['InterFrequencyLoadBalancing'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional7.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['InterFrequencyLoadBalancing_recomend'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional8.set_index('SiteID').to_dict()['featureState']
    iflb['AutoCellCapEstFunction'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = optional8.set_index('SiteID').to_dict()['featureState_recomend']
    iflb['AutoCellCapEstFunction_recomend'] = iflb['Siteid'].map(dict_fitur)

    #UeMeasControl
    dict_fitur = UeMeasControl.set_index('CellId').to_dict()['ueMeasurementsActiveIF']
    iflb['ueMeasurementsActiveIF'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = UeMeasControl.set_index('CellId').to_dict()['ueMeasurementsActiveIF_recomend']
    iflb['ueMeasurementsActiveIF_recomend'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = UeMeasControl.set_index('CellId').to_dict()['ueMeasurementsActive']
    iflb['ueMeasurementsActive'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = UeMeasControl.set_index('CellId').to_dict()['ueMeasurementsActive_recomend']
    iflb['ueMeasurementsActive_recomend'] = iflb['Cellid'].map(dict_fitur)

    #EUtranCellFDD
    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['cellSubscriptionCapacity']
    iflb['cellSubscriptionCapacity'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['cellSubscriptionCapacity_recomend']
    iflb['cellSubscriptionCapacity_recomend'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['covTriggerdBlindHoAllowed']
    iflb['covTriggerdBlindHoAllowed'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellFDD.set_index('CellId').to_dict()['covTriggerdBlindHoAllowed_recomend']
    iflb['covTriggerdBlindHoAllowed_recomend'] = iflb['Cellid'].map(dict_fitur)

    #EUtranCellTDD
    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['cellSubscriptionCapacity']
    iflb['tddcellSubscriptionCapacity'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['cellSubscriptionCapacity_recomend']
    iflb['tddcellSubscriptionCapacity_recomend'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['covTriggerdBlindHoAllowed']
    iflb['tddcovTriggerdBlindHoAllowed'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranCellTDD.set_index('CellId').to_dict()['covTriggerdBlindHoAllowed_recomend']
    iflb['tddcovTriggerdBlindHoAllowed_recomend'] = iflb['Cellid'].map(dict_fitur)

    #LoadBalancingFunction
    dict_fitur = LoadBalancingFunction.set_index('SiteID').to_dict()['lbThreshold']
    iflb['lbThreshold'] = iflb['Siteid'].map(dict_fitur)

    dict_fitur = LoadBalancingFunction.set_index('SiteID').to_dict()['lbThreshold_recomend']
    iflb['lbThreshold_recomend'] = iflb['Siteid'].map(dict_fitur)

    #ReportConfigA5
    dict_fitur = ReportConfigA5.set_index('CellId').to_dict()['a5Threshold1Rsrp']
    iflb['a5Threshold1Rsrp'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigA5.set_index('CellId').to_dict()['a5Threshold1Rsrp_recomend']
    iflb['a5Threshold1Rsrp_recomend'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigA5.set_index('CellId').to_dict()['a5Threshold2Rsrp']
    iflb['a5Threshold2Rsrp'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigA5.set_index('CellId').to_dict()['a5Threshold2Rsrp_recomend']
    iflb['a5Threshold2Rsrp_recomend'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigA5.set_index('CellId').to_dict()['hysteresisA5']
    iflb['hysteresisA5'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigA5.set_index('CellId').to_dict()['hysteresisA5_recomend']
    iflb['hysteresisA5_recomend'] = iflb['Cellid'].map(dict_fitur)

    #ReportConfigEUtraInterFreqLb
    dict_fitur = ReportConfigEUtraInterFreqLb.set_index('CellId').to_dict()['a5Threshold1Rsrp']
    iflb['a5Threshold1Rsrp_FreqLb'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigEUtraInterFreqLb.set_index('CellId').to_dict()['a5Threshold1Rsrp_recomend']
    iflb['a5Threshold1Rsrp_FreqLb_recomend'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigEUtraInterFreqLb.set_index('CellId').to_dict()['a5Threshold2Rsrp']
    iflb['a5Threshold2Rsrp_FreqLb'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = ReportConfigEUtraInterFreqLb.set_index('CellId').to_dict()['a5Threshold2Rsrp_recomend']
    iflb['a5Threshold2Rsrp_FreqLb_recomend'] = iflb['Cellid'].map(dict_fitur)

    #EUtranFreqRelation
    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['lbActivationThreshold']
    iflb['275lbActivationThreshold'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['lbActivationThreshold_recomend']
    iflb['275lbActivationThreshold_recomend'] = iflb['Cellid'].map(dict_fitur)
    
    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['lbA5Thr1RsrpFreqOffset']
    iflb['275lbA5Thr1RsrpFreqOffset'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation1.set_index('CellId').to_dict()['lbA5Thr1RsrpFreqOffset_recomend']
    iflb['275lbA5Thr1RsrpFreqOffset_recomend'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['lbActivationThreshold']
    iflb['1850lbActivationThreshold'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['lbActivationThreshold_recomend']
    iflb['1850lbActivationThreshold_recomend'] = iflb['Cellid'].map(dict_fitur)
    
    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['lbA5Thr1RsrpFreqOffset']
    iflb['1850lbA5Thr1RsrpFreqOffset'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation2.set_index('CellId').to_dict()['lbA5Thr1RsrpFreqOffset_recomend']
    iflb['1850lbA5Thr1RsrpFreqOffset_recomend'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['lbActivationThreshold']
    iflb['3500lbActivationThreshold'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['lbActivationThreshold_recomend']
    iflb['3500lbActivationThreshold_recomend'] = iflb['Cellid'].map(dict_fitur)
    
    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['lbA5Thr1RsrpFreqOffset']
    iflb['3500lbA5Thr1RsrpFreqOffset'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation3.set_index('CellId').to_dict()['lbA5Thr1RsrpFreqOffset_recomend']
    iflb['3500lbA5Thr1RsrpFreqOffset_recomend'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['lbActivationThreshold']
    iflb['38750lbActivationThreshold'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['lbActivationThreshold_recomend']
    iflb['38750lbActivationThreshold_recomend'] = iflb['Cellid'].map(dict_fitur)
    
    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['lbA5Thr1RsrpFreqOffset']
    iflb['38750lbA5Thr1RsrpFreqOffset'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation4.set_index('CellId').to_dict()['lbA5Thr1RsrpFreqOffset_recomend']
    iflb['38750lbA5Thr1RsrpFreqOffset_recomend'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['lbActivationThreshold']
    iflb['38894lbActivationThreshold'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['lbActivationThreshold_recomend']
    iflb['38894lbActivationThreshold_recomend'] = iflb['Cellid'].map(dict_fitur)
    
    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['lbA5Thr1RsrpFreqOffset']
    iflb['38894lbA5Thr1RsrpFreqOffset'] = iflb['Cellid'].map(dict_fitur)

    dict_fitur = EUtranFreqRelation5.set_index('CellId').to_dict()['lbA5Thr1RsrpFreqOffset_recomend']
    iflb['38894lbA5Thr1RsrpFreqOffset_recomend'] = iflb['Cellid'].map(dict_fitur)

    # Merge feature&OptionalFeature EUtranCellFDD&EUtranCellTDD
    iflb = fitting.state(iflb, "CXC4011663", "PshoBasedCsfbToUtran")
    iflb = fitting.state(iflb, "CXC4011664", "MeasBasedCsfbTargetSelection")
    iflb = fitting.state(iflb, "CXC4011554", "IFLBActivationThreshold")
    iflb = fitting.state(iflb, "CXC4010770", "InterFreqSessionContinuity")
    iflb = fitting.state(iflb, "CXC4010974", "InterFrequencyLteHandover")
    iflb = fitting.state(iflb, "CXC4011443", "IntraLTEInterModeHandover")
    iflb = fitting.state(iflb, "CXC4011319", "InterFrequencyLoadBalancing")
    iflb = fitting.state(iflb, "CXC4011373", "AutoCellCapEstFunction")
    iflb = fitting.eutran(iflb, "cellSubscriptionCapacity")
    iflb = fitting.eutran(iflb, "covTriggerdBlindHoAllowed")

    iflb = fitting.cellSubscriptionCapacity(iflb)

    iflb = fitting.check(iflb)
    return iflb