import pandas as pd
import numpy as np
import sys
import os
import traceback
import numbers
import re
from datetime import datetime

python_file_path = os.path.dirname(os.path.realpath(__file__))

def fitting_template_3G():
    global python_file_path
    # check available regional decoded_cm
    j = 0
    wilayah = ["SUMBAGUT", "SUMBAGTENG", "KALIMANTAN"]
    region = []
    for daerah in wilayah:
        for file_list in os.listdir("%s/decoded_cm/parameter/%s" %(python_file_path, daerah)):
            if ".csv" in file_list and j == 0:
                region.append(daerah)
                j = 1
        j=0
        
    # Getting cell id, site id, earfcn
    df_data = pd.DataFrame()
    df_info_id = pd.DataFrame()
    
    #gabungin csv seluruh region
    for regional in region:
        df_temp = pd.read_csv("%s/decoded_cm/parameter/%s/UtranCell.csv" %(python_file_path, regional), delimiter=",", index_col=None, header='infer', low_memory=False)
        df_temp['Regional'] = regional
        
        df_data = pd.concat([df_data,df_temp], axis=0, ignore_index=True, sort=False)
    
    df_info_id['Siteid'] = np.nan
    df_info_id['Cellid'] = df_data['utrancellid']
    df_info_id['waw'] = df_info_id['Cellid'].str[:6]
    df_info_id['Siteid'] = df_info_id['waw']
    df_info_id = df_info_id.drop(['waw'], axis=1)
    df_info_id['RNC'] = df_data['subnetwork']


    df_info_id["code"] = df_info_id['Siteid'].str[0:3]
    df_regional = pd.read_csv("%s/mapping_regional.csv" %python_file_path, delimiter=",", index_col=None, header='infer',low_memory=False)
    dict_regional = df_regional.set_index('kode').to_dict()['regional']
    df_info_id['regional'] = df_info_id['code'].map(dict_regional)

    df_info_id = df_info_id.drop(['code'], axis=1)
    
    del df_data, df_temp
    return df_info_id    

def fitting_template():  
    global python_file_path
    # check available regional decoded_cm
    j = 0
    wilayah = ["SUMBAGUT", "SUMBAGTENG", "KALIMANTAN"]
    region = []
    for daerah in wilayah:
        for file_list in os.listdir("%s/decoded_cm/parameter/%s" %(python_file_path, daerah)):
            if ".csv" in file_list and j == 0:
                region.append(daerah)
                j = 1
        j=0
    
    # Getting cell id, site id, earfcn
    df_fdd_file = pd.DataFrame()
    df_tdd_file = pd.DataFrame()
    df_fdd = pd.DataFrame()
    df_tdd = pd.DataFrame()
    
    #gabungin csv seluruh region
    for regional in region:
        df_temp_fdd = pd.read_csv("%s/decoded_cm/parameter/%s/EUtranCellFDD.csv" %(python_file_path, regional), delimiter=",", index_col=None, header='infer', low_memory=False)
        df_temp_fdd['Regional'] = regional
        df_temp_tdd = pd.read_csv("%s/decoded_cm/parameter/%s/EUtranCellTDD.csv"%(python_file_path, regional), delimiter=",", index_col=None, header='infer', low_memory=False)
        df_temp_tdd['Regional'] = regional
        
        df_fdd = pd.concat([df_fdd,df_temp_fdd], axis=0, ignore_index=True, sort=False)
        df_tdd = pd.concat([df_tdd,df_temp_tdd], axis=0, ignore_index=True, sort=False)
    
    df_fdd_file['NE ID'] = np.nan
    df_fdd_file['Siteid'] = np.nan
    df_fdd_file['Cellid'] = df_fdd['eutrancellfddid']
    df_fdd_file['regional'] = np.nan
    df_fdd_file['earfcn'] = df_fdd['earfcndl']
    df_fdd_file['dlChannelBandwidth'] = df_fdd['dlChannelBandwidth']

    df_tdd_file['NE ID'] = np.nan
    df_tdd_file['Siteid'] = np.nan
    df_tdd_file['Cellid'] = df_tdd['eutrancelltddid']
    df_tdd_file['regional'] = np.nan
    df_tdd_file['earfcn'] = df_tdd['earfcn']
    df_tdd_file['dlChannelBandwidth'] = df_tdd['channelBandwidth']

    df_info_id = pd.concat([df_fdd_file,df_tdd_file], axis=0, ignore_index=True, sort=False)

    df_info_id['Siteid'] = df_info_id['Cellid'].str[:6]
    df_info_id['NE ID'] = df_info_id['Cellid'].str[:8]
    
    df_info_id["code"] = df_info_id['Siteid'].str[0:3]
    df_regional = pd.read_csv("%s/mapping_regional.csv" %python_file_path, delimiter=",", index_col=None, header='infer',low_memory=False)
    dict_regional = df_regional.set_index('kode').to_dict()['regional']
    df_info_id['regional'] = df_info_id['code'].map(dict_regional)

    df_info_id = df_info_id.drop(['code'], axis=1)
    
    del df_fdd_file, df_tdd_file, df_temp_fdd, df_temp_tdd, df_fdd, df_tdd 
    return df_info_id

def mo_filter(mo, parameter, template):
    df = pd.read_csv("%s/audit_result/parameter/%s/detailed/%s.csv" %(python_file_path, template, mo), delimiter=",", index_col=None, header='infer', low_memory=False)
    if mo == "FeatureState":
        df = df.loc[df['featurestateid'] == parameter]
    elif mo == "OptionalFeatureLicense":
        df = df.loc[df['optionalfeaturelicenseid'] == parameter]
    elif mo == "EUtranFreqRelation":
        df = df.loc[df['eutranfreqrelationid'] == parameter]
    elif mo == "QciProfilePredefined":
        df = df.loc[df['qciprofilepredefinedid'] == parameter]
    return df

def mo_read(mo, template):
    if mo == "SectorCarrier":
        df = pd.read_csv("%s/audit_result/parameter/%s/detailed/SectorCarrier_revised.csv" %(python_file_path, template), delimiter=",", index_col=None, header='infer', low_memory=False)
    else :
        df = pd.read_csv("%s/audit_result/parameter/%s/detailed/%s.csv" %(python_file_path, template, mo), delimiter=",", index_col=None, header='infer', low_memory=False)
    return df
    
def state(dataframe, fitur, optional, template):

    # getting param list
    df_template = pd.read_csv("%s/%s.csv" %(python_file_path, template), delimiter=",", index_col=None, header='infer')
    df_parameter = df_template.loc[df_template['MO'] == "FeatureState"]
    param = df_parameter['Parameter'].unique()

    for parameter in param:
    
        dataframe['temp'] = np.nan
        dataframe.loc[pd.notnull(dataframe["%s_%s" %(fitur, parameter)]) , 'temp'] = dataframe["%s_%s" %(fitur, parameter)]
        dataframe.loc[pd.notnull(dataframe["%s_%s" %(optional, parameter)]) , 'temp'] = dataframe["%s_%s" %(optional, parameter)]
        dataframe["%s_%s" %(fitur, parameter)] = dataframe['temp']
        dataframe = dataframe.rename(columns={"%s_%s" %(fitur, parameter) : "%s_%s/%s" %(parameter, fitur, optional)})
        
        dataframe = dataframe.drop(columns=['temp', "%s_%s" %(optional, parameter)], axis=1)
        
        dataframe['temp'] = np.nan
        dataframe.loc[pd.notnull(dataframe["%s_%s_recomend" %(fitur, parameter)]) , 'temp'] = dataframe["%s_%s_recomend" %(fitur, parameter)]
        dataframe.loc[pd.notnull(dataframe["%s_%s_recomend" %(optional, parameter)]) , 'temp'] = dataframe["%s_%s_recomend" %(optional, parameter)]
        dataframe["%s_%s_recomend" %(fitur, parameter)] = dataframe['temp']
        dataframe = dataframe.rename(columns={"%s_%s_recomend" %(fitur, parameter) : "%s_%s/%s_recomend" %(parameter, fitur, optional)})
        
        dataframe = dataframe.drop(columns=['temp', "%s_%s_recomend" %(optional, parameter)], axis=1)
    
    return dataframe

def eutran(dataframe, template):
    # getting param list
    df_template = pd.read_csv("%s/%s.csv" %(python_file_path, template), delimiter=",", index_col=None, header='infer')
    df_parameter = df_template.loc[df_template['MO'] == "EUtranCellFDD"]
    param = df_parameter['Parameter'].unique()

    for fitur in param:
        dataframe['temp'] = np.nan
        dataframe.loc[pd.notnull(dataframe[fitur]) , 'temp'] = dataframe[fitur]
        dataframe.loc[pd.notnull(dataframe["tdd%s" %fitur]) , 'temp'] = dataframe["tdd%s" %fitur]
        dataframe[fitur] = dataframe['temp']
        dataframe = dataframe.drop(columns=['temp'], axis=1)

        if "info" in dataframe["%s_recomend" %fitur].unique():
            dataframe["%s_recomend" %fitur] = "info"
            dataframe = dataframe.drop(columns=["tdd%s" %fitur, "tdd%s_recomend" %fitur], axis=1)
        else:
            dataframe['temp'] = np.nan
            dataframe.loc[pd.notnull(dataframe["%s_recomend" %fitur]) , 'temp'] = dataframe["%s_recomend" %fitur]
            dataframe.loc[pd.notnull(dataframe["tdd%s_recomend" %fitur]) , 'temp'] = dataframe["tdd%s_recomend" %fitur]
            dataframe["%s_recomend" %fitur] = dataframe['temp']
        
            dataframe = dataframe.drop(columns=["tdd%s" %fitur, "tdd%s_recomend" %fitur, 'temp'], axis=1)

    #combine sib3 and eutrancell
    if ("sNonIntraSearch" in dataframe.columns) and ("systemInformationBlock3_sNonIntraSearch" in dataframe.columns):
        dataframe['temp'] = np.nan
        dataframe.loc[pd.notnull(dataframe["systemInformationBlock3_sNonIntraSearch"]) , 'temp'] = dataframe["systemInformationBlock3_sNonIntraSearch"]
        dataframe.loc[pd.notnull(dataframe["sNonIntraSearch"]) , 'temp'] = dataframe["sNonIntraSearch"]
        dataframe["sNonIntraSearch"] = dataframe['temp']
        dataframe = dataframe.drop(columns=['temp'], axis=1)
        dataframe = dataframe.drop(columns=["systemInformationBlock3_sNonIntraSearch"], axis=1)

    return dataframe

def param_eutran(dataframe, param, template):
    for fitur in param:
        dataframe['temp'] = np.nan
        dataframe.loc[pd.notnull(dataframe[fitur]) , 'temp'] = dataframe[fitur]
        dataframe.loc[pd.notnull(dataframe["tdd%s" %fitur]) , 'temp'] = dataframe["tdd%s" %fitur]
        dataframe[fitur] = dataframe['temp']
        dataframe = dataframe.drop(columns=['temp'], axis=1)

        if "info" in dataframe["%s_recomend" %fitur].unique():
            dataframe["%s_recomend" %fitur] = "info"
            dataframe = dataframe.drop(columns=["tdd%s" %fitur, "tdd%s_recomend" %fitur], axis=1)
        else:
            dataframe['temp'] = np.nan
            dataframe.loc[pd.notnull(dataframe["%s_recomend" %fitur]) , 'temp'] = dataframe["%s_recomend" %fitur]
            dataframe.loc[pd.notnull(dataframe["tdd%s_recomend" %fitur]) , 'temp'] = dataframe["tdd%s_recomend" %fitur]
            dataframe["%s_recomend" %fitur] = dataframe['temp']
        
            dataframe = dataframe.drop(columns=["tdd%s" %fitur, "tdd%s_recomend" %fitur, 'temp'], axis=1)
    return dataframe

def check(dataframe):
    checklist = list(dataframe.columns)
    checklist.remove('Cellid')
    checklist.remove('earfcn')
    checklist.remove('Siteid')
    checklist.remove('dlChannelBandwidth')
    checklist.remove('regional')
    
    for notes in checklist:
        if "_recomend" in notes:
            checklist.remove(notes)
    
    for checking in checklist:
        dataframe['%s_check' %checking] = "nok"
        
        if (checking == 'cellRange'):
            dataframe.loc[dataframe[checking] <= 15,'%s_check' %checking] = "ok"
        elif (("info" in dataframe["%s_recomend" %checking].unique()) and (checking != 'cellSubscriptionCapacity') and (checking != 'cellRange')  and (checking != 'pdschTypeBGain')):
            dataframe['%s_check' %checking] = "ok"
        else:
            dataframe.loc[dataframe[checking] == dataframe["%s_recomend" %checking],'%s_check' %checking] = "ok"
        
        dataframe["%s_recomend" %checking] = dataframe['%s_check' %checking]
        dataframe = dataframe.drop(columns=['%s_check' %checking], axis=1)
    
    checklist = list(dataframe.columns)
    summary = []
    
    for notes in checklist:
        if "_recomend" in notes:
            summary.append(notes)
            
    dataframe['checklist'] = "ok"
    
    for notes in summary:
        dataframe.loc[dataframe[notes] == "nok", 'checklist'] = "nok"
    
    return dataframe

def cellSubscriptionCapacity(dataframe):
    dataframe['cellSubscriptionCapacity_recomend'] = np.nan
    dataframe.loc[(dataframe['earfcn']==1850) & (dataframe['dlChannelBandwidth']==20000),'cellSubscriptionCapacity_recomend'] = 34000
    dataframe.loc[(dataframe['earfcn']==1850) & (dataframe['dlChannelBandwidth']==15000),'cellSubscriptionCapacity_recomend'] = 25500
    dataframe.loc[(dataframe['earfcn']==1850) & (dataframe['dlChannelBandwidth']==10000),'cellSubscriptionCapacity_recomend'] = 17000
    dataframe.loc[(dataframe['earfcn']==1850) & (dataframe['dlChannelBandwidth']==5000),'cellSubscriptionCapacity_recomend'] = 8500
    dataframe.loc[(dataframe['earfcn']==3500) & (dataframe['dlChannelBandwidth']==10000),'cellSubscriptionCapacity_recomend'] = 17000
    dataframe.loc[(dataframe['earfcn']==3500) & (dataframe['dlChannelBandwidth']==5000),'cellSubscriptionCapacity_recomend'] = 8500
    dataframe.loc[(dataframe['earfcn']==38750) & (dataframe['dlChannelBandwidth']==20000),'cellSubscriptionCapacity_recomend'] = 24000
    dataframe.loc[(dataframe['earfcn']==38894) & (dataframe['dlChannelBandwidth']==10000),'cellSubscriptionCapacity_recomend'] = 12000
    dataframe.loc[(dataframe['earfcn']==250) & (dataframe['dlChannelBandwidth']==10000),'cellSubscriptionCapacity_recomend'] = 17000
    dataframe.loc[(dataframe['earfcn']==275) & (dataframe['dlChannelBandwidth']==5000),'cellSubscriptionCapacity_recomend'] = 8500
    return dataframe

def maxRange(dataframe):
    dataframe['pdschTypeBGain_recomend'] = np.nan
    dataframe.loc[dataframe['crsGain'] == 0 , 'pdschTypeBGain_recomend'] = 0
    dataframe.loc[dataframe['crsGain'] == 177 , 'pdschTypeBGain_recomend'] = 0
    dataframe.loc[dataframe['crsGain'] == 300 , 'pdschTypeBGain_recomend'] = 1
    dataframe.loc[dataframe['crsGain'] == 477 , 'pdschTypeBGain_recomend'] = 2
    dataframe.loc[dataframe['crsGain'] == 600 , 'pdschTypeBGain_recomend'] = 3
    dataframe.loc[dataframe['crsGain'] == -100 , 'pdschTypeBGain_recomend'] = 0
    dataframe.loc[dataframe['crsGain'] == -200 , 'pdschTypeBGain_recomend'] = 0
    dataframe.loc[dataframe['crsGain'] == -300 , 'pdschTypeBGain_recomend'] = 0
    return dataframe

def mo_type1(layering, mo, template):
    print(mo)
    df = mo_read(mo, template)

    df_template = pd.read_csv("%s/%s.csv" %(python_file_path, template), delimiter=",", index_col=None, header='infer')
    df_parameter = df_template.loc[df_template['MO'] == mo]
    param = df_parameter['Parameter'].unique()

    for parameter in param:
        if mo == "EUtranCellTDD":
            dict_fitur = df.set_index('CellId').to_dict()[parameter]
            layering['tdd%s' %parameter] = layering['Cellid'].map(dict_fitur)

            dict_fitur = df.set_index('CellId').to_dict()['%s_recomend' %parameter]
            layering['tdd%s_recomend' %parameter] = layering['Cellid'].map(dict_fitur)

        elif mo == "ReportConfigA5" or mo == "ReportConfigEUtraInterFreqLb":
            dict_fitur = df.set_index('CellId').to_dict()[parameter]
            layering["%s_%s" %(mo, parameter)] = layering['Cellid'].map(dict_fitur)

            dict_fitur = df.set_index('CellId').to_dict()['%s_recomend' %parameter]
            layering['%s_%s_recomend' %(mo, parameter)] = layering['Cellid'].map(dict_fitur)

        elif mo == "EUtranCellFDD" or mo == "ReportConfigEUtraBadCovPrim" or mo == "ReportConfigSearch" or mo == "UeMeasControl" or mo == "SectorCarrier"  or mo == "RbsLocalCell" or mo == "UtranCell" or mo == "Hsdsch" or mo == "Eul" or mo == "MultiCarrier" or mo == "NodeBLocalCell":           
            dict_fitur = df.set_index('CellId').to_dict()[parameter]
            layering[parameter] = layering['Cellid'].map(dict_fitur)

            dict_fitur = df.set_index('CellId').to_dict()['%s_recomend' %parameter]
            layering['%s_recomend' %parameter] = layering['Cellid'].map(dict_fitur)

        else:
            dict_fitur = df.set_index('SiteID').to_dict()[parameter]
            layering[parameter] = layering['Siteid'].map(dict_fitur)

            dict_fitur = df.set_index('SiteID').to_dict()['%s_recomend' %parameter]
            layering['%s_recomend' %parameter] = layering['Siteid'].map(dict_fitur)

    print("%s completed!" %mo)
    return layering

def mo_type3(layering, mo, template):
    print(mo)
    # getting param list
    df_template = pd.read_csv("%s/%s.csv" %(python_file_path, template), delimiter=",", index_col=None, header='infer')
    df_parameter = df_template.loc[df_template['MO'] == mo]
    param = df_parameter['Parameter'].unique()

    # getting condition param
    if mo == "EUtranFreqRelation":
        condition_param = df_parameter['condition_value_int'].unique()
    else:
        condition_param = df_parameter['condition_value'].unique()

    for condition in condition_param:
        for parameter in param:
            df = mo_filter(mo, condition, template)

            if mo == "EUtranFreqRelation":
                dict_fitur = df.set_index('CellId').to_dict()[parameter]
                layering['%s%s' %(condition, parameter)] = layering['Cellid'].map(dict_fitur)

                dict_fitur = df.set_index('CellId').to_dict()['%s_recomend' %parameter]
                layering['%s%s_recomend' %(condition, parameter)] = layering['Cellid'].map(dict_fitur)
            else:
                dict_fitur = df.set_index('SiteID').to_dict()[parameter]
                layering['%s_%s' %(condition, parameter)] = layering['Siteid'].map(dict_fitur)

                dict_fitur = df.set_index('SiteID').to_dict()['%s_recomend' %parameter]
                layering['%s_%s_recomend' %(condition, parameter)] = layering['Siteid'].map(dict_fitur)
    print("%s completed!" %mo)
    return layering

def type1(layering, mo, parameter, template):
    df = mo_read(mo, template)

    if mo == "EUtranCellTDD":
        dict_fitur = df.set_index('CellId').to_dict()[parameter]
        layering['tdd%s' %parameter] = layering['Cellid'].map(dict_fitur)

        dict_fitur = df.set_index('CellId').to_dict()['%s_recomend' %parameter]
        layering['tdd%s_recomend' %parameter] = layering['Cellid'].map(dict_fitur)

    elif mo == "ReportConfigA5" or mo == "ReportConfigEUtraInterFreqLb":
        dict_fitur = df.set_index('CellId').to_dict()[parameter]
        layering["%s_%s" %(mo, parameter)] = layering['Cellid'].map(dict_fitur)

        dict_fitur = df.set_index('CellId').to_dict()['%s_recomend' %parameter]
        layering['%s_%s_recomend' %(mo, parameter)] = layering['Cellid'].map(dict_fitur)

    elif mo == "EUtranCellFDD" or mo == "ReportConfigEUtraBadCovPrim" or mo == "ReportConfigSearch" or mo == "UeMeasControl" or mo == "SectorCarrier"  or mo == "RbsLocalCell" or mo == "UtranCell" or mo == "Hsdsch" or mo == "Eul" or mo == "MultiCarrier" or mo == "NodeBLocalCell":           
        dict_fitur = df.set_index('CellId').to_dict()[parameter]
        layering[parameter] = layering['Cellid'].map(dict_fitur)

        dict_fitur = df.set_index('CellId').to_dict()['%s_recomend' %parameter]
        layering['%s_recomend' %parameter] = layering['Cellid'].map(dict_fitur)

    else:
        dict_fitur = df.set_index('SiteID').to_dict()[parameter]
        layering[parameter] = layering['Siteid'].map(dict_fitur)

        dict_fitur = df.set_index('SiteID').to_dict()['%s_recomend' %parameter]
        layering['%s_recomend' %parameter] = layering['Siteid'].map(dict_fitur)

    print("%s : %s Completed!" %(mo, parameter))
    return layering

def type3(layering, mo, parameter, condition, template):
    df = mo_filter(mo, condition, template)
    if mo == "EUtranFreqRelation":
        dict_fitur = df.set_index('CellId').to_dict()[parameter]
        layering['%s%s' %(condition, parameter)] = layering['Cellid'].map(dict_fitur)

        dict_fitur = df.set_index('CellId').to_dict()['%s_recomend' %parameter]
        layering['%s%s_recomend' %(condition, parameter)] = layering['Cellid'].map(dict_fitur)
    else:
        dict_fitur = df.set_index('SiteID').to_dict()[parameter]
        layering['%s_%s' %(condition, parameter)] = layering['Siteid'].map(dict_fitur)

        dict_fitur = df.set_index('SiteID').to_dict()['%s_recomend' %parameter]
        layering['%s_%s_recomend' %(condition, parameter)] = layering['Siteid'].map(dict_fitur)

    print("%s : %s(%s) Completed!" %(mo, parameter, condition))
    return layering

def no_recomend(dataframe):
    checklist = list(dataframe.columns)
    for col in checklist:
        if "_recomend" in col:
            dataframe = dataframe.drop(columns=[col], axis=1)
    return dataframe

def get_sectorCarrier_cell(template):  
    global python_file_path
    # check available regional decoded_cm
    j = 0
    wilayah = ["SUMBAGUT", "SUMBAGTENG", "KALIMANTAN"]
    region = []
    for daerah in wilayah:
        for file_list in os.listdir("%s/decoded_cm/parameter/%s" %(python_file_path, daerah)):
            if ".csv" in file_list and j == 0:
                region.append(daerah)
                j = 1
        j=0
    
    # Getting cell id, site id, earfcn
    df_fdd_file = pd.DataFrame()
    df_tdd_file = pd.DataFrame()
    df_fdd = pd.DataFrame()
    df_tdd = pd.DataFrame()
    
    #gabungin csv seluruh region
    python_file_path = os.path.dirname(os.path.realpath(__file__))
    for regional in region:
        df_temp_fdd = pd.read_csv("%s/decoded_cm/parameter/%s/EUtranCellFDD.csv" %(python_file_path, regional), delimiter=",", index_col=None, header='infer', low_memory=False)
        df_temp_tdd = pd.read_csv("%s/decoded_cm/parameter/%s/EUtranCellTDD.csv"%(python_file_path, regional), delimiter=",", index_col=None, header='infer', low_memory=False)
        
        df_fdd = pd.concat([df_fdd,df_temp_fdd], axis=0, ignore_index=True, sort=False)
        df_tdd = pd.concat([df_tdd,df_temp_tdd], axis=0, ignore_index=True, sort=False)
    
    df_fdd_file['mecontext'] = df_fdd['mecontext']
    df_fdd_file['Cellid'] = df_fdd['eutrancellfddid']
    df_fdd_file['Siteid'] = np.nan
    df_fdd_file['sectorCarrierRef'] = df_fdd['sectorCarrierRef']

    df_tdd_file['mecontext'] = df_tdd['mecontext']
    df_tdd_file['Cellid'] = df_tdd['eutrancelltddid']
    df_tdd_file['Siteid'] = np.nan
    df_tdd_file['sectorCarrierRef'] = df_tdd['sectorCarrierRef']

    df_info_id = pd.concat([df_fdd_file,df_tdd_file], axis=0, ignore_index=True, sort=False)

    df_info_id['Siteid'] = df_info_id['Cellid'].str[:6]

    df_list_cell = df_info_id

    df_list_cell['LastDigit'] = df_list_cell['sectorCarrierRef'].str.strip().str[-2:]
    df_list_cell['LastDigit'] = df_list_cell['LastDigit'].str.replace(r'\D', '')

    df_sector = pd.read_csv("%s/audit_result/parameter/%s/detailed/SectorCarrier.csv" %(python_file_path, template), delimiter=",", index_col=None, header='infer',low_memory=False)
    df_sector['LastDigit'] = df_sector['sectorFunctionRef'].str.strip().str[-2:]
    df_sector['LastDigit'] = df_sector['LastDigit'].str.replace(r'\D', '')
    mecontext_filter = df_sector['mecontext'].unique()

    i = 0
    for mecontext in mecontext_filter:
        df_temp_sector = df_sector.loc[df_sector["mecontext"] == mecontext]
        df_temp_cell = df_list_cell.loc[df_list_cell["mecontext"] == mecontext]
        
        dict_carrier = df_temp_cell.set_index('LastDigit').to_dict()['Cellid']
        df_temp_sector['CellId'] = df_temp_sector['LastDigit'].map(dict_carrier)
        
        if i == 0:
            df = df_temp_sector
            i = 1
        elif i == 1:
            df = pd.concat([df,df_temp_sector], axis=0, ignore_index=True, sort=False)
    df = df.drop(columns=["LastDigit"], axis=1)
    df.to_csv("%s/audit_result/parameter/%s/detailed/SectorCarrier_revised.csv" %(python_file_path, template), index=False)
    
    del df_fdd_file, df_tdd_file, df_temp_fdd, df_temp_tdd, df_fdd, df_tdd