import fitting
import pandas as pd
import numpy as np
import sys
import os
import traceback
import numbers
import re
from datetime import datetime
import warnings
warnings.filterwarnings("ignore")

def QciProfilePredefined(template):
    python_file_path = os.path.dirname(os.path.realpath(__file__))
    QciProfilePredefined = pd.read_csv("%s/audit_result/parameter/%s/detailed/QciProfilePredefined.csv" %(python_file_path, template), delimiter=",", index_col=None, header='infer', low_memory=False)
    QciProfilePredefined = QciProfilePredefined.drop(columns=['mecontext', 'subnetwork', 'MO', 'Regional'], axis=1)
    QciProfilePredefined = fitting.no_recomend(QciProfilePredefined)
    QciProfilePredefined.sort_values(['SiteID', 'qciprofilepredefinedid'], inplace=True, ascending=True)
    QciProfilePredefined = QciProfilePredefined.fillna("#NULL")
    return QciProfilePredefined

def Paging(template):
    Paging = fitting.fitting_template()
    Paging = fitting.mo_type1(Paging, "Paging", template)
    Paging = fitting.no_recomend(Paging)
    Paging = Paging.fillna("#NULL")
    return Paging

def DL_Thp_Improvement(template):
    DL_Thp_Improvement = fitting.fitting_template()

    #DL Power
    fitting.get_sectorCarrier_cell(template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "SectorCarrier", "configuredMaxTxPower", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "SectorCarrier", "maximumTransmissionPower", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellFDD", "crsGain", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellTDD", "crsGain", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellFDD", "pdschTypeBGain", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellTDD", "pdschTypeBGain", template)

    #ELC
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "featureState", "CXC4011984", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "licenseState", "CXC4011984", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "featureState", "EricssonLeanCarrier", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "licenseState", "EricssonLeanCarrier", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellFDD", "elcEnabled", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellTDD", "elcEnabled", template)
    DL_Thp_Improvement['elcActivityThreshold'] = np.nan
    DL_Thp_Improvement['elcConfigurationThreshold'] = np.nan

    #DLComp
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "featureState", "CXC4011913", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "licenseState", "CXC4011913", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "featureState", "DlComp", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "licenseState", "DlComp", template)
    #DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "DlComp", "dlCompMeasRprtMaxSinr", template)
    #DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "DlComp", "dlCompMeasRprtMinSinr", template)
    DL_Thp_Improvement['dlCompMeasRprtMaxSinr'] = np.nan
    DL_Thp_Improvement['dlCompMeasRprtMinSinr'] = np.nan

    #PDCCH Power Boost & PDCCH Coverage Extension
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "featureState", "CXC4011515", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "licenseState", "CXC4011515", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "featureState", "PdcchPowerBoost", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "licenseState", "PdcchPowerBoost", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellFDD", "pdcchPowerBoostMax", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellTDD", "pdcchPowerBoostMax", template) 
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellFDD", "pdcchCovImproveSrb", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellTDD", "pdcchCovImproveSrb", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellFDD", "pdcchCfiMode", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellTDD", "pdcchCfiMode", template)

    #MIMO
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellFDD", "transmissionMode", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellTDD", "transmissionMode", template)          
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "SectorCarrier", "noOfRxAntennas", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "SectorCarrier", "noOfTxAntennas", template)

    #256 QAM
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "featureState", "CXC4011969", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "licenseState", "CXC4011969", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "featureState", "Dl256Qam", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "licenseState", "Dl256Qam", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "SectorCarrier", "radioTransmitPerformanceMode", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellFDD", "dl256QamEnabled", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "EUtranCellTDD", "dl256QamEnabled", template)

    #Carrier Aggregation
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "featureState", "CXC4011476", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "licenseState", "CXC4011476", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "featureState", "CarrierAggregation", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "licenseState", "CarrierAggregation", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "featureState", "CXC4011559", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "licenseState", "CXC4011559", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "featureState", "DynamicScellSelection", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "licenseState", "DynamicScellSelection", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "featureState", "CXC4012097", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "licenseState", "CXC4012097", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "featureState", "ConfigurableScellPrio", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "licenseState", "ConfigurableScellPrio", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "featureState", "CXC4011714", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "licenseState", "CXC4011714", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "featureState", "ThreeDlCarrierAggregation", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "licenseState", "ThreeDlCarrierAggregation", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "featureState", "CXC4011922", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "FeatureState", "licenseState", "CXC4011922", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "featureState", "CarrierAggregationFddTdd", template)
    DL_Thp_Improvement = fitting.type3(DL_Thp_Improvement, "OptionalFeatureLicense", "licenseState", "CarrierAggregationFddTdd", template)

    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "CarrierAggregationFunction", "caUsageLimit", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "CarrierAggregationFunction", "dynamicSCellSelectionMethod", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "CarrierAggregationFunction", "laaSCellActProhibitTimer", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "CarrierAggregationFunction", "laaSCellDeactProhibitTimer", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "CarrierAggregationFunction", "sCellActDeactDataThres", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "CarrierAggregationFunction", "sCellActDeactDataThresHyst", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "CarrierAggregationFunction", "sCellActProhibitTimer", template)
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "CarrierAggregationFunction", "sCellScheduleSinrThres", template)	
    DL_Thp_Improvement = fitting.type1(DL_Thp_Improvement, "CarrierAggregationFunction", "sCellSelectionMode", template)

    DL_Thp_Improvement = fitting.param_eutran(DL_Thp_Improvement, ["crsGain","pdschTypeBGain","elcEnabled", "pdcchPowerBoostMax", "pdcchCovImproveSrb", "pdcchCfiMode", "transmissionMode", "dl256QamEnabled"], template)

    DL_Thp_Improvement = fitting.state(DL_Thp_Improvement, "CXC4011984", "EricssonLeanCarrier", template)
    DL_Thp_Improvement = fitting.state(DL_Thp_Improvement, "CXC4011913", "DlComp", template)
    DL_Thp_Improvement = fitting.state(DL_Thp_Improvement, "CXC4011515", "PdcchPowerBoost", template)
    DL_Thp_Improvement = fitting.state(DL_Thp_Improvement, "CXC4011969", "Dl256Qam", template)
    DL_Thp_Improvement = fitting.state(DL_Thp_Improvement, "CXC4011476", "CarrierAggregation", template)
    DL_Thp_Improvement = fitting.state(DL_Thp_Improvement, "CXC4011559", "DynamicScellSelection", template)
    DL_Thp_Improvement = fitting.state(DL_Thp_Improvement, "CXC4012097", "ConfigurableScellPrio", template)
    DL_Thp_Improvement = fitting.state(DL_Thp_Improvement, "CXC4011714", "ThreeDlCarrierAggregation", template)
    DL_Thp_Improvement = fitting.state(DL_Thp_Improvement, "CXC4011922", "CarrierAggregationFddTdd", template)

    DL_Thp_Improvement = DL_Thp_Improvement.drop(columns=["licenseState_CXC4011476/CarrierAggregation", "licenseState_CXC4011559/DynamicScellSelection", "licenseState_CXC4011714/ThreeDlCarrierAggregation", "licenseState_CXC4011922/CarrierAggregationFddTdd", "licenseState_CXC4012097/ConfigurableScellPrio"], axis=1)
		        
    DL_Thp_Improvement = fitting.no_recomend(DL_Thp_Improvement)
    DL_Thp_Improvement = DL_Thp_Improvement.fillna("#NULL")
    return DL_Thp_Improvement

def UL_Thp_Improvement(template):
    UL_Thp_Improvement = fitting.fitting_template()

    #UlComp
    UL_Thp_Improvement = fitting.type3(UL_Thp_Improvement, "FeatureState", "featureState", "CXC4011444", template)
    UL_Thp_Improvement = fitting.type3(UL_Thp_Improvement, "FeatureState", "licenseState", "CXC4011444", template)
    UL_Thp_Improvement = fitting.type3(UL_Thp_Improvement, "OptionalFeatureLicense", "featureState", "UlComP", template)
    UL_Thp_Improvement = fitting.type3(UL_Thp_Improvement, "OptionalFeatureLicense", "licenseState", "UlComP", template)
    UL_Thp_Improvement = fitting.type1(UL_Thp_Improvement, "UlCompGroup", "operationalState", template)

    #64 QAM Uplink
    UL_Thp_Improvement = fitting.type3(UL_Thp_Improvement, "FeatureState", "featureState", "CXC4011946", template)
    UL_Thp_Improvement = fitting.type3(UL_Thp_Improvement, "FeatureState", "licenseState", "CXC4011946", template)
    UL_Thp_Improvement = fitting.type3(UL_Thp_Improvement, "OptionalFeatureLicense", "featureState", "Ul64Qam", template)
    UL_Thp_Improvement = fitting.type3(UL_Thp_Improvement, "OptionalFeatureLicense", "licenseState", "Ul64Qam", template)
    UL_Thp_Improvement = fitting.type1(UL_Thp_Improvement, "EUtranCellFDD", "ul64qamEnabled", template)
    UL_Thp_Improvement = fitting.type1(UL_Thp_Improvement, "EUtranCellTDD", "ul64qamEnabled", template)
    UL_Thp_Improvement = fitting.type1(UL_Thp_Improvement, "EUtranCellFDD", "puschPwrOffset64qam", template)
    UL_Thp_Improvement = fitting.type1(UL_Thp_Improvement, "EUtranCellTDD", "puschPwrOffset64qam", template)

    #UL pwr ctrl
    UL_Thp_Improvement = fitting.type1(UL_Thp_Improvement, "EUtranCellFDD", "alpha", template)
    UL_Thp_Improvement = fitting.type1(UL_Thp_Improvement, "EUtranCellTDD", "alpha", template)
    UL_Thp_Improvement = fitting.type1(UL_Thp_Improvement, "EUtranCellFDD", "pZeroNominalPucch", template)
    UL_Thp_Improvement = fitting.type1(UL_Thp_Improvement, "EUtranCellTDD", "pZeroNominalPucch", template)    
    UL_Thp_Improvement = fitting.type1(UL_Thp_Improvement, "EUtranCellFDD", "pZeroNominalPusch", template)
    UL_Thp_Improvement = fitting.type1(UL_Thp_Improvement, "EUtranCellTDD", "pZeroNominalPusch", template) 

    UL_Thp_Improvement = fitting.param_eutran(UL_Thp_Improvement, ["ul64qamEnabled", "puschPwrOffset64qam", "alpha", "pZeroNominalPucch", "pZeroNominalPusch"], template)

    UL_Thp_Improvement = fitting.state(UL_Thp_Improvement, "CXC4011444", "UlComP", template)
    UL_Thp_Improvement = fitting.state(UL_Thp_Improvement, "CXC4011946", "Ul64Qam", template)
    		        
    UL_Thp_Improvement = fitting.no_recomend(UL_Thp_Improvement)
    UL_Thp_Improvement = UL_Thp_Improvement.fillna("#NULL")
    return UL_Thp_Improvement

def CS_Improvement(template):
    CS_Improvement = fitting.fitting_template()

    CS_Improvement = fitting.type3(CS_Improvement, "FeatureState", "featureState", "CXC4010956", template)
    CS_Improvement = fitting.type3(CS_Improvement, "FeatureState", "licenseState", "CXC4010956", template)
    CS_Improvement = fitting.type3(CS_Improvement, "OptionalFeatureLicense", "featureState", "CsfbToGeranUtran", template)
    CS_Improvement = fitting.type3(CS_Improvement, "OptionalFeatureLicense", "licenseState", "CsfbToGeranUtran", template)

    CS_Improvement = fitting.type3(CS_Improvement, "FeatureState", "featureState", "CXC4011034", template)
    CS_Improvement = fitting.type3(CS_Improvement, "FeatureState", "licenseState", "CXC4011034", template)
    CS_Improvement = fitting.type3(CS_Improvement, "OptionalFeatureLicense", "featureState", "EcsCsfb", template)
    CS_Improvement = fitting.type3(CS_Improvement, "OptionalFeatureLicense", "licenseState", "EcsCsfb", template)

    #RedirectWithNacc
    CS_Improvement = fitting.type3(CS_Improvement, "FeatureState", "featureState", "CXC4010973", template)
    CS_Improvement = fitting.type3(CS_Improvement, "FeatureState", "licenseState", "CXC4010973", template)
    CS_Improvement = fitting.type3(CS_Improvement, "OptionalFeatureLicense", "featureState", "RedirectWithNacc", template)
    CS_Improvement = fitting.type3(CS_Improvement, "OptionalFeatureLicense", "licenseState", "RedirectWithNacc", template)

    CS_Improvement = fitting.type1(CS_Improvement, "AnrFunctionUtran", "rimIntegrationEnabled", template)
    CS_Improvement = fitting.type1(CS_Improvement, "AnrFunctionUtran", "anrStateUtran", template)

    CS_Improvement = fitting.type1(CS_Improvement, "ENodeBFunction", "maxNoCellsNaccCsfb", template)

    CS_Improvement = fitting.state(CS_Improvement, "CXC4010956", "CsfbToGeranUtran", template)
    CS_Improvement = fitting.state(CS_Improvement, "CXC4011034", "EcsCsfb", template)
    CS_Improvement = fitting.state(CS_Improvement, "CXC4010973", "RedirectWithNacc", template)
    		        
    CS_Improvement = fitting.no_recomend(CS_Improvement)
    CS_Improvement = CS_Improvement.fillna("#NULL")
    return CS_Improvement


'''
def Cell(template):
    Cell = fitting.fitting_template()
    Cell = fitting.type1(Cell, "EUtranCellFDD", template)
    Cell = fitting.type1(Cell, "EUtranCellTDD", template)
    Cell = fitting.eutran(Cell, template)
    Cell = fitting.no_recomend(Cell)
    Cell = Cell.fillna("#NULL")
    return Cell

def FeatureState(template):
    FeatureState = fitting.fitting_template()
    FeatureState = fitting.type3(FeatureState, "FeatureState", template)
    FeatureState = fitting.type3(FeatureState, "OptionalFeatureLicense", template)

    FeatureState = fitting.state(FeatureState, "CXC4011984", "EricssonLeanCarrier", template)
    FeatureState = fitting.state(FeatureState, "CXC4011913", "DlComp", template)
    FeatureState = fitting.state(FeatureState, "CXC4011515", "PdcchPowerBoost", template)
    FeatureState = fitting.state(FeatureState, "CXC4011969", "Dl256Qam", template)
    FeatureState = fitting.state(FeatureState, "CXC4011476", "CarrierAggregation", template)
    FeatureState = fitting.state(FeatureState, "CXC4011559", "DynamicScellSelection", template)
    FeatureState = fitting.state(FeatureState, "CXC4012097", "ConfigurableScellPrio", template)
    FeatureState = fitting.state(FeatureState, "CXC4011714", "ThreeDlCarrierAggregation", template)
    FeatureState = fitting.state(FeatureState, "CXC4011922", "CarrierAggregationFddTdd", template)
    FeatureState = fitting.state(FeatureState, "CXC4011444", "UlComP", template)
    FeatureState = fitting.state(FeatureState, "CXC4011946", "Ul64Qam", template)
    FeatureState = fitting.state(FeatureState, "CXC4010956", "CsfbToGeranUtran", template)
    FeatureState = fitting.state(FeatureState, "CXC4011034", "EcsCsfb", template)
    FeatureState = fitting.state(FeatureState, "CXC4010973", "RedirectWithNacc", template)
    FeatureState = fitting.no_recomend(FeatureState)
    FeatureState = FeatureState.fillna("#NULL")
    return FeatureState

def CarrierAggregationFunction(template):
    CarrierAggregationFunction = fitting.fitting_template()
    CarrierAggregationFunction = fitting.type1(CarrierAggregationFunction, "CarrierAggregationFunction", template)
    CarrierAggregationFunction = fitting.no_recomend(CarrierAggregationFunction)
    CarrierAggregationFunction = CarrierAggregationFunction.fillna("#NULL")
    return CarrierAggregationFunction

def UlCompGroup(template):
    UlCompGroup = fitting.fitting_template()
    UlCompGroup = fitting.type1(UlCompGroup, "UlCompGroup", template)
    UlCompGroup = fitting.no_recomend(UlCompGroup)
    UlCompGroup = UlCompGroup.fillna("#NULL")
    return UlCompGroup

def AnrFunctionUtran(template):
    AnrFunctionUtran = fitting.fitting_template()
    AnrFunctionUtran = fitting.type1(AnrFunctionUtran, "AnrFunctionUtran", template)
    AnrFunctionUtran = fitting.no_recomend(AnrFunctionUtran)
    AnrFunctionUtran = AnrFunctionUtran.fillna("#NULL")
    return AnrFunctionUtran

def ENodeBFunction(template):
    ENodeBFunction = fitting.fitting_template()
    ENodeBFunction = fitting.type1(ENodeBFunction, "ENodeBFunction", template)
    ENodeBFunction = fitting.no_recomend(ENodeBFunction)
    ENodeBFunction = ENodeBFunction.fillna("#NULL")
    return ENodeBFunction

def SectorCarrier(template):
    fitting.get_sectorCarrier_cell(template)
    SectorCarrier = fitting.fitting_template()
    SectorCarrier = fitting.type1(SectorCarrier, "SectorCarrier", template)
    SectorCarrier = fitting.no_recomend(SectorCarrier)
    SectorCarrier = SectorCarrier.fillna("#NULL")
    return SectorCarrier
'''
