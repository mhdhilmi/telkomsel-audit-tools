import re
import os
import xlwt
import sys
from datetime import datetime
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

def call(tech,input_file_path,detailed_file_path,region):
	temporary_file = "%s/temp" %detailed_file_path
	if not os.path.exists(temporary_file):
			os.mkdir(temporary_file)

	# Site level feature audit
	if (tech == "4G"):
		df3 = df_feature_state_4G.pivot_table(index='mecontext', columns='featurestateid', values='servicestate', aggfunc='max')	
	if (tech == "3G"):
		df3 = df_feature_state_3G.pivot_table(index='mecontext', columns='featurestateid', values='servicestate', aggfunc='max')
	if (tech == "CELL"):
		df3 = df_feature_nodeBCell.pivot_table(index='mecontext', columns='featurestateid', values='servicestate', aggfunc='max')
	if (tech == "RNC"):
		df3 = df_feature_rnc.pivot_table(index='mecontext', columns='featurestateid', values='servicestate', aggfunc='max')
	df3.reset_index()
	df3.to_csv("%s/test_4_%s.csv" %(python_file_path, tech), index=True)
	del df3
	
	df_site_feature = pd.read_csv("%s/test_4_%s.csv" %(python_file_path, tech), delimiter=",", index_col=None, header='infer')

	### Getting Site ID and regional
	if (tech == "4G") or (tech == "3G") or (tech == "CELL"):
		df_site_feature['SiteID'] = df_site_feature['mecontext'].str.findall(r'[a-zA-Z][a-zA-Z][a-zA-Z][0-9]+')
		df_site_feature.to_csv("%s/%s_feature_audit_site_%s.csv" %(temporary_file, region, tech), index=False)
		df_site_feature = pd.read_csv("%s/%s_feature_audit_site_%s.csv" %(temporary_file, region, tech), delimiter=",", index_col=None, header='infer',low_memory=False)
		df_site_feature['SiteID'] = df_site_feature['SiteID'].str[2:8]
	elif tech == "RNC":
		df_site_feature['SiteID'] = df_site_feature['mecontext']
	df_site_feature['region'] = region

	### Writting detailed result not final
	df_site_feature.to_csv("%s/%s_feature_audit_site_%s.csv" %(temporary_file, region, tech), index=False)

	del df_site_feature

def regional_change(python_file_path, detailed_file_path, region):
	#Rubah regional sesuai MCOM (gabungin semua dulu)
	tech = ["4G", "3G", "CELL", "RNC"]

	for tech_type in tech:
		j = 0
		for filename in os.listdir("%s/temp" %detailed_file_path):
			if (filename.endswith(".csv")) and (tech_type in filename):
				df = pd.read_csv("%s/temp/%s" %(detailed_file_path, filename), delimiter=",", index_col=None, header='infer',low_memory=False)
				#print("%s : %s" %(filename, len(df)))
				if j == 0:
					df_summary = df
					j = 1
				elif j == 1:
					df_summary = pd.concat([df_summary,df], axis=0, ignore_index=True, sort=False)
				os.remove("%s/temp/%s" %(detailed_file_path, filename))
				
		if (tech_type == "4G") or (tech_type == "3G") or (tech_type == "CELL"):
			df_summary["code"] = df_summary["SiteID"].str[0:3]
			df_regional = pd.read_csv("%s/mapping_regional.csv" %python_file_path, delimiter=",", index_col=None, header='infer',low_memory=False)
			dict_regional = df_regional.set_index('kode').to_dict()['regional']
			df_summary['regional'] = df_summary['code'].map(dict_regional)
			
		elif (tech_type == "RNC"):
			df_summary["code"] = df_summary["mecontext"]
			df_summary['regional'] = df_summary['region']
		
		df_summary = df_summary.drop(['SiteID', 'code', 'region'], axis=1)
		#print("%s : %s" %(tech_type, len(df_summary)))
		df_summary.to_csv("%s/temp/%s.csv" %(detailed_file_path, tech_type), index=False)

	del df, df_summary

	#balikin ke per regional
	#print("Data setelah dipisah lagi per regional")
	changes = 0
	for tech_type in tech:
		df = pd.read_csv("%s/temp/%s.csv" %(detailed_file_path, tech_type), delimiter=",", index_col=None, header='infer',low_memory=False)
		for regional in region:
			df_summary = df.loc[df['regional'] == regional]
			df_summary = df_summary.drop(['regional'], axis=1)
			#print("%s_%s : %s" %(regional, tech_type, len(df_summary)))
			### Checking for previous result
			before = "%s/%s_feature_audit_site_%s.csv" %(detailed_file_path, regional, tech_type)
			if os.path.isfile(before):
				df_old = pd.read_csv("%s/%s_feature_audit_site_%s.csv" %(detailed_file_path, regional, tech_type), delimiter=",", index_col=None, header='infer',low_memory=False)
				df_new = df_summary

				df_new['status'] = 1
				dict_new = df_new.set_index('mecontext').to_dict()['status']
				df_old['stat'] = df_old['mecontext'].map(dict_new)	
				df_add = df_old.loc[pd.isnull(df_old['stat'])]

				if(len(df_add) > 0):
					changes = changes + 1

				df_add = df_add.drop(columns=['stat'], axis=1)
				df_new = df_new.drop(columns=['status'], axis=1)
				df_summary = pd.concat([df_new,df_add], axis=0, ignore_index=True, sort=False)
				del df_new
				del df_old
			elif not os.path.isfile(before):
				df_summary = df_summary

			df_summary.to_csv("%s/%s_feature_audit_site_%s.csv" %(detailed_file_path, regional, tech_type), index=False)
			
		os.remove("%s/temp/%s.csv" %(detailed_file_path, tech_type))
	
	if changes > 0:
		print("++++++++++++++++++++++++++++++++++++++++++++")
		print("Data was added from previous feature audit")
		print("++++++++++++++++++++++++++++++++++++++++++++")
	elif changes == 0:
		print("++++++++++++++++++++++++++++++++++++++++++++")
		print("No data added!")
		print("++++++++++++++++++++++++++++++++++++++++++++")

	del df, df_summary
	os.rmdir("%s/temp" %detailed_file_path)

def summary_file(detailed_file_path, region, tech):
	# Feature audit Summary
	df_summary = pd.read_csv("%s/%s_feature_audit_site_%s.csv" %(detailed_file_path, region, tech), delimiter=",", index_col=None, header='infer',low_memory=False)
	#df_summary = df_summary.drop(df_summary.index[0])
	df_summary = df_summary.drop(['mecontext'], axis=1)

	with open("%s/%s_feature_audit_summary_%s.csv" %(detailed_file_path, region, tech), 'w') as outfile:
		outfile.write("featureId,numSite,numLicense,Enabled,Disable,PercEnable\n")
		for x in df_summary.columns:
			df_summary[x] = pd.to_numeric(df_summary[x], errors='coerce')
			num_1 = len(df_summary.loc[df_summary[x] == 1])
			num_0 = len(df_summary.loc[df_summary[x] == 0])
			num_site = len(df_summary[x])

			if (num_0 + num_1) == 0 :
				perc = 0
			elif (num_0 + num_1) != 0 :
				perc = ((num_1/(num_1+num_0))*100)

			s_temp = '%s,%s,%s,%s,%s,%.2f\n' %(x,num_site, num_1+num_0, num_1, num_0, perc)
			outfile.write(s_temp)
	print("Num of %s %s site : %s" %(tech, region, num_site))
	del df_summary

if __name__ == '__main__':
	print("Feature Audit is starting . . .")
	python_file_path = os.path.dirname(os.path.realpath(__file__))
	t1 = datetime.now()

	if os.path.isfile("%s/feature audit success.txt" %python_file_path):
		os.remove("%s/feature audit success.txt" %python_file_path)

	j = 0
	wilayah = ["SUMBAGUT", "SUMBAGTENG", "KALIMANTAN"]
	region = []
	for daerah in wilayah:
		for file_list in os.listdir("%s/decoded_cm/feature/%s" %(python_file_path, daerah)):
			if ".csv" in file_list and j == 0:
				region.append(daerah)
				j = 1
		j=0 

	for regional in region:
		input_file_path = "%s/decoded_cm/feature/%s" %(python_file_path, regional)
		audit_file_path = "%s/audit_result" %python_file_path

		if not os.path.exists(audit_file_path):
			os.mkdir(audit_file_path)
		
		if not os.path.exists(input_file_path):
			os.mkdir(input_file_path)

		#tempat summary
		summary_file_path = "%s/feature" %audit_file_path
		if not os.path.exists(summary_file_path):
			os.mkdir(summary_file_path)

		#tempat detailed
		detailed_file_path = "%s/detailed" %summary_file_path
		if not os.path.exists(detailed_file_path):
			os.mkdir(detailed_file_path)
		
		os.chdir(input_file_path)
		f_fea_dus = "%s/OptionalFeatureLicense.csv" %input_file_path
		f_fea_bb = "%s/FeatureState.csv" %input_file_path
		f_fea_nodeB = "%s/NodeBFunction.csv" %input_file_path
		f_fea_rbs = "%s/RbsLocalCell.csv" %input_file_path
		f_fea_nodeBcell = "%s/NodeBLocalCell.csv" %input_file_path
		f_fea_rnc = "%s/RncFeature.csv" %input_file_path

		num_data_site = 0
		num_data_cell = 0
		i_dus = 0
		i_bb = 0
		i_nodeB = 0
		i_rbs = 0
		i_nodeBCell = 0

		# MO List
		# 4G --> FeatureState (BB) & OptionalFeatureLicense (DU)
		# 3G --> FeatureState (BB) & NodeBFunction (DU)
		# RNC --> RncFeature
		# Cell --> NodeBLocalCell (BB) & RbsLocalCell (DU)

		# MO OptionalFeatureLicense
		if os.path.isfile(f_fea_dus):
			df_feature_dus_temp = pd.read_csv(f_fea_dus, delimiter=",", index_col=None, header='infer')
			df_feature_dus_temp.columns = [x.lower() for x in df_feature_dus_temp.columns]
			df_feature_dus = df_feature_dus_temp[['mecontext','keyid','userlabel','servicestate','featurestate']]
			del df_feature_dus_temp

			df_feature_dus.columns = ['mecontext', 'featurestateid', 'description', 'servicestate', 'featurestate']
			num_data_site = num_data_site + 1
			i_dus = 1

		# MO FeatureState
		if os.path.isfile(f_fea_bb):
			df_feature_state_temp = pd.read_csv(f_fea_bb, delimiter=",", index_col=None, header='infer')
			df_feature_state_temp.columns = [x.lower() for x in df_feature_state_temp.columns]
			df_feature_state = df_feature_state_temp[['mecontext', 'featurestateid', 'description', 'servicestate', 'featurestate']]

			del df_feature_state_temp
			num_data_site = num_data_site + 1
			i_bb = 1

		# MO NodeBFunction
		if os.path.isfile(f_fea_nodeB):
			df_nodeBF = pd.read_csv(f_fea_nodeB, delimiter=",", index_col=None, header='infer')
			state = 0
			for x in df_nodeBF.columns:
				if x.startswith('feat'):
					if state == 0:
						df_feature_nodeBFunction = df_nodeBF.groupby(['mecontext'], as_index=False).agg({x : 'max'})
						
						df_feature_nodeBFunction['featurestateid'] = x
						df_feature_nodeBFunction['description'] = x
						df_feature_nodeBFunction['featurestate'] = 0
								
						df_feature_nodeBFunction.columns = ['mecontext','servicestate','featurestateid','description','featurestate']
						df_feature_nodeBFunction = df_feature_nodeBFunction[['mecontext','featurestateid','description','servicestate','featurestate']]
						state = 1
					else:
						df_feature_nodeBFunction2 = df_nodeBF.groupby(['mecontext'], as_index=False).agg({x : 'max'})

						df_feature_nodeBFunction2['featurestateid'] = x
						df_feature_nodeBFunction2['description'] = x
						df_feature_nodeBFunction2['featurestate'] = 0

						df_feature_nodeBFunction2.columns = ['mecontext','servicestate','featurestateid','description','featurestate']
						df_feature_nodeBFunction2 = df_feature_nodeBFunction2[['mecontext','featurestateid','description','servicestate','featurestate']]
						df_feature_nodeBFunction = pd.concat([df_feature_nodeBFunction,df_feature_nodeBFunction2], axis=0, ignore_index=True, sort=False)
			
			del df_feature_nodeBFunction2
			df_feature_nodeBFunction.sort_values(by=['mecontext'], inplace=True)

			df_base = pd.read_csv("%s/feature_list.csv" %python_file_path, delimiter=",", index_col=None, header='infer')
			dict_featurestateid = df_base.set_index('NodeBFunction').to_dict()['key_id']
			dict_name = df_base.set_index('NodeBFunction').to_dict()['feature_name']
			df_feature_nodeBFunction['key_id'] = df_feature_nodeBFunction['featurestateid'].map(dict_featurestateid)
			df_feature_nodeBFunction['feature_name'] = df_feature_nodeBFunction['featurestateid'].map(dict_name)

			df_feature_nodeBFunction = df_feature_nodeBFunction.loc[pd.notna(df_feature_nodeBFunction['key_id'])]
			df_feature_nodeBFunction['featurestateid'] = df_feature_nodeBFunction['key_id']
			df_feature_nodeBFunction['description'] = df_feature_nodeBFunction['feature_name']

			df_feature_nodeBFunction = df_feature_nodeBFunction.drop(['key_id','feature_name'], axis=1)

			num_data_site = num_data_site + 1
			i_nodeB = 1

		# MO RbsLocalCell
		if os.path.isfile(f_fea_rbs):
			df_rbs = pd.read_csv(f_fea_rbs, delimiter=",", index_col=None, header='infer')
			state = 0
			#df_rbs.columns = [x.lower() for x in df_rbs.columns]
			for x in df_rbs.columns:
				if x.startswith('featureState'):
					if state == 0:
						df_feature_RBS = df_rbs.groupby(['mecontext'], as_index=False).agg({x : 'max'})
					
						df_feature_RBS['featurestateid'] = x
						df_feature_RBS['description'] = x
						df_feature_RBS['featurestate'] = 0
					
						df_feature_RBS.columns = ['mecontext','servicestate','featurestateid','description','featurestate']
						df_feature_RBS = df_feature_RBS[['mecontext','featurestateid','description','servicestate','featurestate']]
						state = 1
					else:
						df_feature_RBS2 = df_rbs.groupby(['mecontext'], as_index=False).agg({x : 'max'})
						
						df_feature_RBS2['featurestateid'] = x
						df_feature_RBS2['description'] = x
						df_feature_RBS2['featurestate'] = 0
						
						df_feature_RBS2.columns = ['mecontext','servicestate','featurestateid','description','featurestate']
						df_feature_RBS2 = df_feature_RBS2[['mecontext','featurestateid','description','servicestate','featurestate']]
						df_feature_RBS = pd.concat([df_feature_RBS,df_feature_RBS2], axis=0, ignore_index=True, sort=False)
			
			del df_feature_RBS2
			df_feature_RBS.sort_values(by=['featurestateid'], inplace=True)

			df_base = pd.read_csv("%s/feature_list.csv" %python_file_path, delimiter=",", index_col=None, header='infer')
			dict_featurestateid = df_base.set_index('RbsLocalCell').to_dict()['key_id']
			dict_name = df_base.set_index('RbsLocalCell').to_dict()['feature_name']
			df_feature_RBS['key_id'] = df_feature_RBS['featurestateid'].map(dict_featurestateid)
			df_feature_RBS['feature_name'] = df_feature_RBS['featurestateid'].map(dict_name)

			df_feature_RBS = df_feature_RBS.loc[pd.notna(df_feature_RBS['key_id'])]
			df_feature_RBS['featurestateid'] = df_feature_RBS['key_id']
			df_feature_RBS['description'] = df_feature_RBS['feature_name']

			df_feature_RBS = df_feature_RBS.drop(['key_id','feature_name'], axis=1)

			num_data_cell = num_data_cell + 1
			i_rbs = 1

		# MO NodeBLocalCell
		if os.path.isfile(f_fea_nodeBcell):
			df_nodeBCell = pd.read_csv(f_fea_nodeBcell, delimiter=",", index_col=None, header='infer')
			state = 0
			for x in df_nodeBCell.columns:
				if x.startswith('feat'):
					if state == 0:
						df_feature_nodeBCell = df_nodeBCell.groupby(['mecontext'], as_index=False).agg({x : 'max'})
						
						df_feature_nodeBCell['featurestateid'] = x
						df_feature_nodeBCell['description'] = x
						df_feature_nodeBCell['featurestate'] = 0
								
						df_feature_nodeBCell.columns = ['mecontext','servicestate','featurestateid','description','featurestate']
						df_feature_nodeBCell = df_feature_nodeBCell[['mecontext','featurestateid','description','servicestate','featurestate']]
						state = 1
					else:
						df_feature_nodeBCell2 = df_nodeBCell.groupby(['mecontext'], as_index=False).agg({x : 'max'})

						df_feature_nodeBCell2['featurestateid'] = x
						df_feature_nodeBCell2['description'] = x
						df_feature_nodeBCell2['featurestate'] = 0

						df_feature_nodeBCell2.columns = ['mecontext','servicestate','featurestateid','description','featurestate']
						df_feature_nodeBCell2 = df_feature_nodeBCell2[['mecontext','featurestateid','description','servicestate','featurestate']]
						df_feature_nodeBCell2 = pd.concat([df_feature_nodeBCell,df_feature_nodeBCell2], axis=0, ignore_index=True, sort=False)
			del df_feature_nodeBCell2
			df_feature_nodeBCell.sort_values(by=['featurestateid'], inplace=True)

			dict_featurestateid = df_base.set_index('NodeBLocalCell').to_dict()['key_id']
			dict_name = df_base.set_index('NodeBLocalCell').to_dict()['feature_name']
			df_feature_nodeBCell['key_id'] = df_feature_nodeBCell['featurestateid'].map(dict_featurestateid)
			df_feature_nodeBCell['feature_name'] = df_feature_nodeBCell['featurestateid'].map(dict_name)

			df_feature_nodeBCell = df_feature_nodeBCell.loc[pd.notna(df_feature_nodeBCell['key_id'])]
			df_feature_nodeBCell['featurestateid'] = df_feature_nodeBCell['key_id']
			df_feature_nodeBCell['description'] = df_feature_nodeBCell['feature_name']

			num_data_cell = num_data_cell + 1
			i_nodeBCell = 1

		# MO RNC
		if os.path.isfile(f_fea_rnc):
			df_feature_rnc = pd.read_csv(f_fea_rnc, delimiter=",", index_col=None, header='infer')
			df_feature_rnc.columns = [x.lower() for x in df_feature_rnc.columns]
			if 'keyid' in df_feature_rnc.columns:
				df_feature_rnc = df_feature_rnc[['mecontext', 'keyid', 'rncfeatureid', 'servicestate', 'featurestate']]
				df_feature_rnc.columns = ['mecontext','featurestateid','description','servicestate','featurestate']
			elif not 'keyid' in df_feature_rnc.columns:
				df_get_key = pd.read_csv("%s/feature_list.csv" %python_file_path, delimiter=",", index_col=None, header='infer')
				df_key = df_get_key.loc[df_get_key['tech'] == "RNC"]

				dict_key = df_get_key.set_index('key_id').to_dict()['tech']
				df_key = df_key[['feature_name','key_id','tech']]

				dict_key = df_key.set_index('feature_name').to_dict()['key_id']
				df_feature_rnc['keyid'] = df_feature_rnc['rncfeatureid'].map(dict_key)

				df_feature_rnc = df_feature_rnc[['mecontext', 'keyid', 'rncfeatureid', 'servicestate', 'featurestate']]
				df_feature_rnc.columns = ['mecontext','featurestateid','description','servicestate','featurestate']
			
		# combine feature DUS & BB 4G
		if num_data_site == 2:
				df_feature_state = df_feature_state.append(df_feature_dus)
		elif num_data_site == 1:
			if i_dus == 1:
				df_feature_state = df_feature_dus
			elif i_bb == 1:
				df_feature_state = df_feature_state
		elif num_data_site == 0:
			print("no site level license found....")

		# combine feature DUS & BB 3G Cell level
		if num_data_cell == 2:
				df_feature_nodeBCell = df_feature_nodeBCell.append(df_feature_RBS)
		elif num_data_cell == 1:
			if i_rbs == 1:
				df_feature_nodeBCell = df_feature_RBS
			elif i_nodeBCell == 1:
				df_feature_nodeBCell = df_feature_nodeBCell
		elif num_data_cell == 0:
			print("no cell level license found....")

		# Filtering CM data (feature_state) based on 3G or 4G
		## Filtering site (3G/4G)
		df_3g = pd.read_csv("%s/NodeBFunction.csv" %input_file_path, delimiter=",", index_col=None, header='infer', low_memory=False)
		df_4g = pd.read_csv("%s/eNodeBFunction.csv" %input_file_path, delimiter=",", index_col=None, header='infer', low_memory=False)

		df_4g['status'] = 1
		df_3g['status'] = 1

		dict_4g = df_4g.set_index('mecontext').to_dict()['status']
		dict_3g = df_3g.set_index('mecontext').to_dict()['status']
		df_feature_state['tech_4g'] = df_feature_state['mecontext'].map(dict_4g)
		df_feature_state['tech_3g'] = df_feature_state['mecontext'].map(dict_3g)

		df_feature_state_3G = df_feature_state.loc[df_feature_state['tech_3g'] == 1]
		df_feature_state_4G = df_feature_state.loc[df_feature_state['tech_4g'] == 1]

		df_feature_state_4G.loc[df_feature_state_4G['servicestate']==0, 'servicestate_0'] = 1
		df_feature_state_3G.loc[df_feature_state_3G['servicestate']==0, 'servicestate_0'] = 1

		## Filtering License based on 3G/4G from FeatureState
		df_list_fea = pd.read_csv("%s/feature_list.csv" %python_file_path, delimiter=",", index_col=None, header='infer')
		dict_tech = df_list_fea.set_index('key_id').to_dict()['tech']

		df_feature_state_3G['tech'] = df_feature_state_3G['featurestateid'].map(dict_tech)
		df_feature_state_4G['tech'] = df_feature_state_4G['featurestateid'].map(dict_tech)
		df_feature_rnc['tech'] = df_feature_rnc['featurestateid'].map(dict_tech)
		df_feature_nodeBCell['tech'] = df_feature_nodeBCell['featurestateid'].map(dict_tech)

		df_feature_state_3G = df_feature_state_3G.loc[df_feature_state_3G['tech'] == "3G"]
		df_feature_state_4G = df_feature_state_4G.loc[df_feature_state_4G['tech'] == "4G"]
		df_feature_rnc = df_feature_rnc.loc[df_feature_rnc['tech'] == "RNC"]
		df_feature_nodeBCell = df_feature_nodeBCell.loc[df_feature_nodeBCell['tech'] == "CELL"]

		## combine feature BB & DUS 3G
		df_feature_state_3G = pd.concat([df_feature_state_3G,df_feature_nodeBFunction], axis=0, ignore_index=True, sort=False)

		## Function for Proccess site level audit
		call("4G",input_file_path,detailed_file_path,regional)
		call("3G",input_file_path,detailed_file_path,regional)
		call("RNC",input_file_path,detailed_file_path,regional)
		call("CELL",input_file_path,detailed_file_path,regional)

		feature_num_4G = df_feature_state_4G.featurestateid.unique()
		feature_num_3G = df_feature_state_3G.featurestateid.unique()
		feature_num_rnc = df_feature_rnc.featurestateid.unique()
		feature_num_cell = df_feature_nodeBCell.featurestateid.unique()

		print("%s completed" %regional)
		
		os.remove("%s/test_4_3G.csv" %python_file_path)
		os.remove("%s/test_4_4G.csv" %python_file_path)
		os.remove("%s/test_4_RNC.csv" %python_file_path)
		os.remove("%s/test_4_CELL.csv" %python_file_path)

		del df_feature_state
		del df_feature_state_3G
		del df_feature_state_4G

	#Function untuk perbaiki region based on MCOM
	regional_change(python_file_path, detailed_file_path, region)

	#Function untuk summary
	technology = ["4G", "3G", "CELL", "RNC"]
	for regional in region:
		for tech in technology:
			summary_file(detailed_file_path, regional, tech)

	t2 = datetime.now()
	print(t2-t1)
	print("--------------------------")
	
	#printing to excell
	print("Printing to excell ....")
	writer = pd.ExcelWriter('%s/feature_summary.xlsx' %summary_file_path) # pylint: disable=abstract-class-instantiated
	for csvfilename in os.listdir(detailed_file_path):
		if (csvfilename.endswith(".csv")) and ("summary" in csvfilename):
			df = pd.read_csv("%s/%s" %(detailed_file_path, csvfilename), delimiter=",", index_col=None, header='infer',low_memory=False)
			df.to_excel(writer, sheet_name = "%s_%s" %(csvfilename.split('_')[0],csvfilename.split('_')[4]), index=False)
	writer.save()

	# success indicator 
	L = t2.strftime("%d-%b-%Y (%H:%M:%S.%f)")
	os.chdir(python_file_path)
	with open("feature audit success.txt", "w") as outfile:
		outfile.write(L)
	print("completed!")