import pandas as pd
import numpy as np
import sys
import os
import traceback
import numbers
import re
from datetime import datetime
import warnings
warnings.filterwarnings("ignore")

#mapping cell id for 3G
def cell3g(input_file_path, detailed_file_path, df_audit, regional):
    df_audit['CellId'] = df_mo['mecontext'].str.findall(r'[a-zA-Z][a-zA-Z][a-zA-Z][0-9]+')
    df_audit.to_csv("%s/apapun.csv" %detailed_file_path, index=False)
    df_audit = pd.read_csv("%s/apapun.csv" %detailed_file_path, delimiter=",", index_col=None, header='infer',low_memory=False)
    df_audit['CellId'] = df_audit['CellId'].str[2:8]
    os.remove("%s/apapun.csv" %detailed_file_path)
    
    dict1 = {'S4C1' : 'SW1', 'S4C2' : 'SW4', 'S4C3' : 'SW7', 
             'S5C1' : 'SW2', 'S5C2' : 'SW5', 'S5C3' : 'SW8',
             'S6C1' : 'SW3', 'S6C2' : 'SW6', 'S6C3' : 'SW9',
             'S10C1' : 'SW1', 'S10C2' : 'SW4', 'S10C3' : 'SW7'    
            }

    dict2 = {'S1C1' : 'W1', 'S2C1' : 'W2', 'S3C1' : 'W3', 
             'S1C2' : 'W4', 'S2C2' : 'W5', 'S3C2' : 'W6', 
             'S1C3' : 'W7', 'S2C3' : 'W8', 'S3C3' : 'W9', 
             'S4C1' : 'W1', 'S4C2' : 'W4', 'S4C3' : 'W7', 
             'S5C1' : 'W2', 'S5C2' : 'W5', 'S5C3' : 'W8',
             'S6C1' : 'W3', 'S6C2' : 'W6', 'S6C3' : 'W9',
             'S7C1' : 'W1', 'S7C2' : 'W4', 'S7C3' : 'W7',
             'S8C1' : 'W2', 'S8C2' : 'W5', 'S8C3' : 'W8',
             'S9C1' : 'W3', 'S9C2' : 'W6', 'S9C3' : 'W9'         
            }

    dict3 = {'S1C1' : 'MW1', 'S2C1' : 'MW2', 'S3C1' : 'MW3', 
             'S1C2' : 'MX1', 'S2C2' : 'MX2', 'S3C2' : 'MX3', 
             'S1C3' : 'MZ1', 'S2C3' : 'MZ2', 'S3C3' : 'MZ3',
             'S4C1' : 'MW1', 'S4C2' : 'MW1', 'S4C3' : 'MZ1', 
             'S5C1' : 'MW2', 'S5C2' : 'MX2', 'S5C3' : 'MZ2',
             'S6C1' : 'MW3', 'S6C2' : 'MX3', 'S6C3' : 'MZ3',
             'S7C1' : 'MW1', 'S7C2' : 'MX1', 'S7C3' : 'MZ1',
             'S8C1' : 'MW2', 'S8C2' : 'MX2', 'S8C3' : 'MZ2',
             'S9C1' : 'MW3', 'S9C2' : 'MX3', 'S9C3' : 'MZ3'
            }
    
    df_audit = df_audit.replace({"nodeblocalcellid1": dict1, "nodeblocalcellid2": dict2, "nodeblocalcellid3": dict3})
    df_audit['mapping1'] = df_audit['CellId'] + df_audit['nodeblocalcellid1']
    df_audit['mapping2'] = df_audit['CellId'] + df_audit['nodeblocalcellid2']
    df_audit['mapping3'] = df_audit['CellId'] + df_audit['nodeblocalcellid3']
    
    df_list = pd.read_csv("%s/UtranCell.csv" %(input_file_path), delimiter=",", index_col=None, header='infer', low_memory=False)
    df_site = pd.DataFrame()
    df_site['Cellid'] = df_list['utrancellid']
    df_site['lookup'] = df_site['Cellid']
    del df_list
    
    dict_cell = df_site.set_index('Cellid').to_dict()['lookup']
    df_audit['map1'] = df_audit['mapping1'].map(dict_cell)

    dict_cell = df_site.set_index('Cellid').to_dict()['lookup']
    df_audit['map2'] = df_audit['mapping2'].map(dict_cell)

    dict_cell = df_site.set_index('Cellid').to_dict()['lookup']
    df_audit['map3'] = df_audit['mapping3'].map(dict_cell)

    df_audit['Cellid'] = np.nan
    df_audit.loc[pd.notnull(df_audit['map1']), 'Cellid'] = df_audit['mapping1']
    df_audit.loc[pd.notnull(df_audit['map2']), 'Cellid'] = df_audit['mapping2']
    df_audit.loc[pd.notnull(df_audit['map3']), 'Cellid'] = df_audit['mapping3']
    
    df_audit['CellId'] = df_audit['Cellid']
    df_audit = df_audit.drop(columns=['nodeblocalcellid1', 'nodeblocalcellid2', 'nodeblocalcellid3',
                                     'mapping1', 'mapping2', 'mapping3', 'map1', 'map2', 'map3', 'Cellid'], axis=1)
    
    return df_audit

if __name__ == '__main__':
    tech = sys.argv[1]
    template = sys.argv[2]
    python_file_path = os.path.dirname(os.path.realpath(__file__))

    if os.path.isfile("%s/parameter audit success.txt" %python_file_path):
        os.remove("%s/parameter audit success.txt" %python_file_path)
    
    #check available regional decoded_cm
    j = 0
    wilayah = ["SUMBAGUT", "SUMBAGTENG", "KALIMANTAN"]
    region = []
    for daerah in wilayah:
        for file_list in os.listdir("%s/decoded_cm/parameter/%s" %(python_file_path, daerah)):
            if ".csv" in file_list and j == 0:
                region.append(daerah)
                j = 1
        j=0

    #Take out all OSS for 3G (KALIMANTAN)
    if tech == "3G":
        region.remove('KALIMANTAN')

    #select template based on activity
    df_template = pd.read_csv("%s/%s.csv" %(python_file_path, template), delimiter=",", index_col=None, header='infer')

    for regional in region:
        input_file_path = "%s/decoded_cm/parameter/%s" %(python_file_path,regional)
        audit_file_path = "%s/audit_result" %python_file_path

        if not os.path.exists(audit_file_path):
            os.mkdir(audit_file_path)
		
        if not os.path.exists(input_file_path):
            os.mkdir(input_file_path)

        #tempat summary
        summary_file_path = "%s/parameter" %(audit_file_path)
        if not os.path.exists(summary_file_path):
            os.mkdir(summary_file_path)

        #tempat template-template
        template_file_path = "%s/%s" %(summary_file_path, template)
        if not os.path.exists(template_file_path):
            os.mkdir(template_file_path)

        #tempat detailed
        before_file_path = "%s/detailed" %template_file_path
        if not os.path.exists(before_file_path):
            os.mkdir(before_file_path)

        #tempat operasi
        detailed_file_path = "%s/detailed/temp" %template_file_path
        if not os.path.exists(detailed_file_path):
            os.mkdir(detailed_file_path)

        print (regional)
        print ("=======================================================")
        list_cell_level = []
        list_mo = df_template['MO'].unique()
        decoded_mo = []
        print("--------------------------------------")
        for files in os.listdir(input_file_path):
            if files.endswith(".csv"):
                files = (os.path.splitext(files)[0])
                decoded_mo.append(files)

        for mo in list_mo:
            if mo in decoded_mo:  
                #MO without codition_param
                print("MO %s" %mo)
                df_mo = pd.read_csv("%s/%s.csv" %(input_file_path, mo), delimiter=",", index_col=None, header='infer', low_memory=False)
                print(len(df_mo))
                df_parameter = df_template.loc[df_template['MO'] == mo]
                column_names = ['mecontext', 'subnetwork', 'MO']
                df_audit = pd.DataFrame(columns = column_names)

                if df_parameter['condition_param'].isnull().any():
                    # parameter without condition param
                    for param in df_parameter['Parameter'].unique():
                        param = param.replace(" ", "")
                        if param in df_mo.columns:
                            df_audit['mecontext'] = df_mo['mecontext']
                            df_audit['subnetwork'] = df_mo['subnetwork']
                            df_audit['MO'] = mo

                            ## Getting Cell ID (for cell level)
                            if mo == 'NodeBLocalCell' or mo == 'RbsLocalCell':
                                if mo == 'NodeBLocalCell':
                                    localid = 'nodeblocalcellid'
                                elif mo == 'RbsLocalCell':
                                    localid = 'rbslocalcellid'
                                df_audit['nodeblocalcellid1'] = df_mo[localid]
                                df_audit['nodeblocalcellid2'] = df_mo[localid]
                                df_audit['nodeblocalcellid3'] = df_mo[localid]                            
                                df_audit = cell3g(input_file_path, detailed_file_path, df_audit, regional)
                                
                            elif mo == "UtranCell":
                                df_audit['CellId'] = df_mo['utrancellid']
                            elif mo == "Hsdsch" or mo == "Eul" or mo == "MultiCarrier":
                                df_audit['CellId'] = df_mo['utrancellid']
                            elif mo == "SectorCarrier":
                                df_audit['CellId'] = np.nan                         
                            elif ('eutrancellfddid' in df_mo.columns) and ('eutrancelltddid' in df_mo.columns):
                                df_audit['CellId'] = ""
                                df_audit['eutrancellfddid'] = df_mo['eutrancellfddid']
                                df_audit['eutrancelltddid'] = df_mo['eutrancelltddid']                     
                                list_cell_level.append(mo)
                            elif not('eutrancellfddid' in df_mo.columns) and not('eutrancelltddid' in df_mo.columns):
                                df_audit['SiteID'] = df_mo['mecontext'].str.findall(r'[a-zA-Z][a-zA-Z][a-zA-Z][0-9]+')
                                df_audit.to_csv("%s/%s_%s.csv" %(detailed_file_path, regional, mo), index=False)
                                df_audit = pd.read_csv("%s/%s_%s.csv" %(detailed_file_path, regional, mo), delimiter=",", index_col=None, header='infer',low_memory=False)
                                df_audit['SiteID'] = df_audit['SiteID'].str[2:8]
                            elif mo == "EUtranCellFDD":
                                df_audit['CellId'] = df_mo['eutrancellfddid']
                            elif mo == "EUtranCellTDD":
                                df_audit['CellId'] = df_mo['eutrancelltddid']

                            df_audit['Regional'] = regional
                            df_audit[param] = df_mo[param]
                            df_audit["%s_param" %param] = param

                            dict_param = df_parameter.set_index('Parameter').to_dict()['all']
                            df_audit["%s_recomend" %param] = df_audit["%s_param" %param].map(dict_param)

                            df_audit = df_audit.drop(columns=["%s_param" %param], axis=1)
                            df_audit.to_csv("%s/%s_%s.csv" %(detailed_file_path, regional, mo), index=False)
                            print("%s completed!" %param)                        
                        else:
                            print("%s is not found" %param)                                
                else:
                    # Parameter need condition param
                    # Collect All parameter with condition param
                    i = 0
                    condition_param = df_parameter['condition_param'].unique()[0]
                    print('Parameter have condition parameter')
                    #print(condition_param)
                    
                    # pisahkan antara condition value int dan string
                    if mo == "EUtranFreqRelation":
                        term = df_parameter['condition_value_int'].unique()
                    else:
                        term = df_parameter['condition_value'].unique()
                    
                    for condition in term:
                        #print(condition)
                        df_temp = df_mo.loc[df_mo[condition_param] == condition]
                        if i == 0:
                            df_withCondition = df_temp
                            i = 1
                        elif i == 1:
                            df_withCondition = pd.concat([df_withCondition,df_temp], axis=0, ignore_index=True, sort=False)

                    # Getting current CMBulk State
                    for param in df_parameter['Parameter'].unique():
                        if param in df_withCondition.columns:
                            df_audit['mecontext'] = df_withCondition['mecontext']
                            df_audit['subnetwork'] = df_withCondition['subnetwork']
                            df_audit['MO'] = mo

                            ## Getting Cell ID (for cell level)
                            if ('eutrancellfddid' in df_withCondition.columns) and ('eutrancelltddid' in df_withCondition.columns):
                                df_audit['CellId'] = ""
                                df_audit['eutrancellfddid'] = df_withCondition['eutrancellfddid']
                                df_audit['eutrancelltddid'] = df_withCondition['eutrancelltddid']
                                list_cell_level.append(mo)
                            elif not('eutrancellfddid' in df_withCondition.columns) and not('eutrancelltddid' in df_withCondition.columns):
                                df_audit['SiteID'] = df_withCondition['mecontext'].str.findall(r'[a-zA-Z][a-zA-Z][a-zA-Z][0-9]+')
                                df_audit.to_csv("%s/Audit_tessss.csv" %(detailed_file_path), index=False)
                                df_audit = pd.read_csv("%s/Audit_tessss.csv" %(detailed_file_path), delimiter=",", index_col=None, header='infer',low_memory=False)
                                df_audit['SiteID'] = df_audit['SiteID'].str[2:8]

                            df_audit['Regional'] = regional             
                            df_audit[condition_param] = df_withCondition[condition_param]
                            df_audit[param] = df_withCondition[param]
                            df_audit.to_csv("%s/Audit_tessss.csv" %(detailed_file_path), index=False)
                        else:
                            print("%s is not found" %param)

                    del df_audit
                    del df_temp

                    # getting recomend state (from parameter template) 
                    df_audit = pd.read_csv("%s/Audit_tessss.csv" %(detailed_file_path), delimiter=",", index_col=None, header='infer')

                    for spesific_value in df_audit[condition_param].unique():
                        df_temp = df_audit.loc[df_audit[condition_param] == spesific_value]
                        if mo == "EUtranFreqRelation":
                            df_parameter_temp = df_parameter.loc[df_parameter['condition_value_int'] == spesific_value]
                        else:
                            df_parameter_temp = df_parameter.loc[df_parameter['condition_value'] == spesific_value]

                        for param in df_parameter['Parameter'].unique():
                            if param in df_withCondition.columns:
                                df_temp["%s_param" %param] = param
                                dict_param = df_parameter_temp.set_index('Parameter').to_dict()['all']
                                df_temp["%s_recomend" %param] = df_temp["%s_param" %param].map(dict_param)

                                df_temp = df_temp.drop(columns=["%s_param" %param], axis=1)
                                df_temp.to_csv("%s/Audit_tessss_result_%s.csv" %(detailed_file_path, spesific_value), index=False)
                            else:
                                print("%s is not found" %param)
                    # Combine Files
                    j=0
                    for spesific_value in df_audit[condition_param].unique():
                        df = pd.read_csv("%s/Audit_tessss_result_%s.csv" %(detailed_file_path, spesific_value), delimiter=",", index_col=None, header='infer')
                        if j == 0:
                            df_final = df
                            j = 1
                        elif j == 1:
                            df_final = pd.concat([df_final,df], axis=0, ignore_index=True, sort=False)
                    df_final.to_csv("%s/%s_%s.csv" %(detailed_file_path, regional, mo), index=False)

                    # Delete unused files
                    for spesific_value in df_audit[condition_param].unique():
                        os.remove("%s/Audit_tessss_result_%s.csv" %(detailed_file_path, spesific_value))

                    if len(df_withCondition) == len(df_final):
                        print('Finish, All clear!')
                    else:
                        print('Different!')
                        print('initial : %s' %df_withCondition)
                        print('output : %s' %df_final)
                    os.remove("%s/Audit_tessss.csv" %detailed_file_path)
                    del df
                    del df_final
            else:
                print("MO %s is not found" %mo)
            print("--------------------------------------")

        # Gabungin tdd dan fdd
        my_set = set(list_cell_level)
        list_cell = list(my_set)

        for cell in list_cell:
            df_relation = pd.read_csv("%s/%s_%s.csv" %(detailed_file_path, regional, cell), delimiter=",", index_col=None, header='infer', low_memory=False)
            if 'eutrancellfddid' in df_relation.columns:
                df_relation_fdd = df_relation.loc[pd.notna(df_relation['eutrancellfddid'])]
                df_relation_tdd = df_relation.loc[pd.notna(df_relation['eutrancelltddid'])]

                df_relation_fdd['EUtranCELL'] = df_relation_fdd['eutrancellfddid']
                df_relation_tdd['EUtranCELL'] = df_relation_tdd['eutrancelltddid']

                df_temp = pd.concat([df_relation_fdd,df_relation_tdd], axis=0, ignore_index=True, sort=False)
                df_cellid = df_temp.drop(columns=['eutrancellfddid', 'eutrancelltddid'], axis=1)

                df_cellid['CellId'] = df_cellid['EUtranCELL']
                df_cellid = df_cellid.drop(columns=['EUtranCELL'], axis=1)

                df_cellid.to_csv("%s/%s_%s.csv" %(detailed_file_path, regional, cell), index=False)
        #del df_temp
        print ("=======================================================")

    print('Audit Complete!')

    # Gabungin semua regional
    menagedObject_list = []

    for files in os.listdir(detailed_file_path):
        if ("_" in files) and (files.endswith(".csv")):
            not_region = (files.split('_')[1])
            menagedObject = not_region.split('.')[0]
            menagedObject_list.append(menagedObject)

    # Getting unique list only
    my_setting = set(menagedObject_list)
    list_of_menagedObject = list(my_setting)           
            
    for mo in list_of_menagedObject:
        df_temp = pd.DataFrame()
        naming = "%s.csv" %mo
        print(mo)
        print("-----------------------------")
        for files in os.listdir(detailed_file_path):
            if "_" in files and ".csv" in files:
                if files.split('_')[1] == naming:
                    df = pd.read_csv("%s/%s" %(detailed_file_path, files), delimiter=",", index_col=None, header='infer', low_memory=False)
                    print("%s : %s" %(files,len(df)))
                    df_temp = pd.concat([df_temp,df], axis=0, ignore_index=True, sort=False)
                    os.remove("%s/%s" %(detailed_file_path, files))
                
        df_temp.to_csv("%s/%s.csv" %(detailed_file_path, mo), index=False)
        print("panjang temp : %s" %len(df_temp))
        print("-----------------------------")
        del df, df_temp
    print("COMPLETED !")

    #Filter technology
    if tech == "4G":
        filt_tech = 'LTE'
    elif tech == "3G":
        filt_tech = 'RN|RV'

    print("filter is %s" %filt_tech)

    for files in os.listdir(detailed_file_path):
        if not "_" in files and ".csv" in files:
            df = pd.read_csv("%s/%s" %(detailed_file_path, files), delimiter=",", index_col=None, header='infer', low_memory=False)
            print("%s : %s" %(files,len(df)))
            #df = df[df["subnetwork"].str.contains('RN|RV', na=False)]
            df = df[df["subnetwork"].str.contains(filt_tech, na=False)]
            print("panjang temp : %s" %len(df))
            df.to_csv("%s/%s" %(detailed_file_path, files), index=False)
    print("completed")

    # comparing file before
    print("Comparing previous file")
    changes = 0
    for files in os.listdir(detailed_file_path):
        if ".csv" in files:
            before = "%s/%s" %(before_file_path, files)
            if os.path.isfile(before):
                #print(before)
                #print("Previous %s found !" %files)
                df_old = pd.read_csv("%s/%s" %(before_file_path, files), delimiter=",", index_col=None, header='infer',low_memory=False)
                df_new = pd.read_csv("%s/%s" %(detailed_file_path, files), delimiter=",", index_col=None, header='infer',low_memory=False)
                df_new['status'] = 1
                dict_new = df_new.set_index('mecontext').to_dict()['status']
                df_old['stat'] = df_old['mecontext'].map(dict_new)	
                df_add = df_old.loc[pd.isnull(df_old['stat'])]

                if(len(df_add) > 0):
                    changes = changes + 1

                df_add = df_add.drop(columns=['stat'], axis=1)
                df_new = df_new.drop(columns=['status'], axis=1)
                df_summary = pd.concat([df_new,df_add], axis=0, ignore_index=True, sort=False)
                del df_new
                del df_old
            else:
                df_summary = pd.read_csv("%s/%s" %(detailed_file_path, files), delimiter=",", index_col=None, header='infer',low_memory=False)
                #print("no previous data")
            
            os.remove("%s/%s" %(detailed_file_path, files))
            df_summary.to_csv("%s/%s" %(before_file_path, files), index=False)
    os.rmdir(detailed_file_path)
            
    if changes > 0:
        print("++++++++++++++++++++++++++++++++++++++++++++")
        print("New data found, updating . . . ")
        print("++++++++++++++++++++++++++++++++++++++++++++")
    elif changes == 0:
        print("++++++++++++++++++++++++++++++++++++++++++++")
        print("No new data")
        print("++++++++++++++++++++++++++++++++++++++++++++")
    print("Comparing data completed")

    # fitting to template
    print("Fitting data to template")
    if template == "layering_LTE":
        import layering_LTE
        audit = layering_LTE.layering(template)
        # write to xslx
        writer = pd.ExcelWriter('%s/%s.xlsx' %(summary_file_path, template)) # pylint: disable=abstract-class-instantiated
        audit.to_excel(writer, sheet_name = template, index=False)
        writer.save()

    elif template == "weekly_4G":
        import weekly_4G
        qciProfilePredefined = weekly_4G.QciProfilePredefined(template)
        paging = weekly_4G.Paging(template)
        DL_Thp_Improvement = weekly_4G.DL_Thp_Improvement(template)
        UL_Thp_Improvement = weekly_4G.UL_Thp_Improvement(template)
        CS_Improvement = weekly_4G.CS_Improvement(template)

        '''
        cell = weekly_4G.Cell(template)
        featureState= weekly_4G.FeatureState(template)
        carrierAggregationFunction = weekly_4G.CarrierAggregationFunction(template)
        ulCompGroup = weekly_4G.UlCompGroup(template)
        anrFunctionUtran = weekly_4G.AnrFunctionUtran(template)
        eNodeBFunction = weekly_4G.ENodeBFunction(template)
        SectorCarrier = weekly_4G.SectorCarrier(template)
        '''

        # write to xslx
        writer = pd.ExcelWriter('%s/%s.xlsx' %(summary_file_path, template)) # pylint: disable=abstract-class-instantiated
        qciProfilePredefined.to_excel(writer, sheet_name = "QciProfilePredefined", index=False)
        paging.to_excel(writer, sheet_name = "Paging", index=False)
        DL_Thp_Improvement.to_excel(writer, sheet_name = "DL_Thp_Improvement", index=False)
        UL_Thp_Improvement.to_excel(writer, sheet_name = "UL_Thp_Improvement", index=False)
        CS_Improvement.to_excel(writer, sheet_name = "CS_Improvement", index=False)
        writer.save()

        '''
        cell.to_excel(writer, sheet_name = "cell", index=False)
        featureState.to_excel(writer, sheet_name = "FeatureState", index=False)
        carrierAggregationFunction.to_excel(writer, sheet_name = "CarrierAggregationFunction", index=False)
        ulCompGroup.to_excel(writer, sheet_name = "UlCompGroup", index=False)
        anrFunctionUtran.to_excel(writer, sheet_name = "AnrFunctionUtran", index=False)
        eNodeBFunction.to_excel(writer, sheet_name = "ENodeBFunction", index=False)
        SectorCarrier.to_excel(writer, sheet_name = "SectorCarrier", index=False)
        '''
        

    elif template == "weekly_3G":
        import weekly_3G
        HS_Thp_Improvement = weekly_3G.HS_Thp_Improvement(template)
        EUL_Thp_Improvement =  weekly_3G.EUL_Thp_Improvement(template)
        Access_and_retain = weekly_3G.Access_and_retain(template)
        RwR_to_LTE =  weekly_3G.RwR_to_LTE(template)

        # write to xslx
        writer = pd.ExcelWriter('%s/%s.xlsx' %(summary_file_path, template)) # pylint: disable=abstract-class-instantiated
        HS_Thp_Improvement.to_excel(writer, sheet_name = "HS_Thp_Improvement", index=False)
        EUL_Thp_Improvement.to_excel(writer, sheet_name = "EUL_Thp_Improvement", index=False)
        Access_and_retain.to_excel(writer, sheet_name = "Access_and_retain", index=False)
        RwR_to_LTE.to_excel(writer, sheet_name = "RwR_to_LTE", index=False)
        writer.save()

    
    print("fitting succes!")

    t2 = datetime.now()

    file1 = open("parameter audit success.txt","w") 
    L = t2.strftime("%d-%b-%Y (%H:%M:%S.%f)")
    file1.write(L)
    file1.close()