import fitting
import pandas as pd
import numpy as np
import sys
import os
import traceback
import numbers
import re
from datetime import datetime
import warnings
warnings.filterwarnings("ignore")

python_file_path = os.path.dirname(os.path.realpath(__file__))

def layering(template):
    global python_file_path
    layering = fitting.fitting_template()
    layering = fitting.mo_type1(layering, "EUtranCellFDD", template)
    layering = fitting.mo_type1(layering, "EUtranCellTDD", template)
    layering = fitting.mo_type1(layering, "ReportConfigA5", template)
    layering = fitting.mo_type1(layering, "ReportConfigEUtraInterFreqLb", template)
    layering = fitting.mo_type1(layering, "ReportConfigEUtraBadCovPrim", template)
    layering = fitting.mo_type1(layering, "ReportConfigSearch", template)
    layering = fitting.mo_type1(layering, "UeMeasControl", template)
    layering = fitting.mo_type1(layering, "LoadBalancingFunction", template)
    layering = fitting.mo_type3(layering, "QciProfilePredefined", template)
    layering = fitting.mo_type3(layering, "EUtranFreqRelation", template)
    layering = fitting.eutran(layering, template)
    layering = fitting.no_recomend(layering)
    layering = layering.fillna("#NULL")
    return layering