import re
import os
from datetime import datetime
import sys
import xml.etree.ElementTree as ET
import re
import os
import math
from datetime import datetime


def check_mo(mo_name, s_mo_id, s_subnetwork, s_mecontext, s_file, vsdata_dict, vsdata_status, output_folder):
    if mo_name not in list_mo:
        lowermo = [x.lower() for x in list_mo]
        mo_name_lower = mo_name.lower()
        if mo_name_lower in lowermo:
            i = lowermo.index(mo_name_lower)
            mo_name = list_mo[i]
        else:
            new_mo = {}
            if mo_name is None:
                print("None")
                filename = mo_name
            else:
                filename = re.sub('vsData','', mo_name)
            new_mo['filename'] = "%s/%s_1.csv" % (output_folder,filename)
            #print(new_mo['filename'])
            new_mo['io'] = open(new_mo['filename'], "wb+")

            new_mo['parameter'] = {}
            new_mo['parameter']['filename'] = s_file
            new_mo['parameter']['mecontext'] = s_mecontext
            new_mo['parameter']['subnetwork'] = s_subnetwork
            new_mo['id_name'] = {}
            
            new_mo['header'] = ['filename','mecontext', 'subnetwork']
            if b_utrancell:
                new_mo['parameter']['utrancell'] = "s_utrancell"
                new_mo['header'].append('utrancell')
            
            if vsdata_status >= 1:
                num_id = 1 + int(math.log10(vsdata_status))
            else:
                num_id = 0
            
            i_status = 0
            for a in range(0, num_id):
                i_status = 10 * i_status + 1
                if vsdata_dict[i_status]['name'] == None:
                    new_mo['parameter']['unknown_id'] = vsdata_dict[i_status]['value']
                    if 'unknown_id' not in new_mo['header']:
                        new_mo['header'].append('unknown_id')
                else:
                    new_mo['id_name'][i_status] = vsdata_dict[i_status]['name']
                    new_mo['parameter'][vsdata_dict[i_status]['name']] = vsdata_dict[i_status]['value']
                    new_mo['header'].append(vsdata_dict[i_status]['name'])
            dict_mo[mo_name] = new_mo
            list_mo.append(mo_name)
    else:
        for key,value in dict_mo[mo_name]['parameter'].items():
            dict_mo[mo_name]['parameter'][key] = ''
        dict_mo[mo_name]['parameter']['filename'] = s_file
        dict_mo[mo_name]['parameter']['mecontext'] = s_mecontext
        dict_mo[mo_name]['parameter']['subnetwork'] = s_subnetwork
        if b_utrancell:
            dict_mo[mo_name]['parameter']['utrancell'] = "s_utrancell"
        
        if vsdata_status >= 1:
            num_id = 1 + int(math.log10(vsdata_status))
        else:
            num_id = 0

        i_status = 0
        for a in range(0, num_id):
            i_status = 10 * i_status + 1
            if vsdata_dict[i_status]['name'] == None:
                if 'unknown_id' in dict_mo[mo_name]['header']:
                    dict_mo[mo_name]['parameter']['unknown_id'] = vsdata_dict[i_status]['value']
                else:
                    dict_mo[mo_name]['header'].append('unknown_id')
                    dict_mo[mo_name]['parameter']['unknown_id'] = vsdata_dict[i_status]['value']
            else:
                #check header
                if vsdata_dict[i_status]['name'] not in dict_mo[mo_name]['header']:
                    dict_mo[mo_name]['header'].append(vsdata_dict[i_status]['name'])
                dict_mo[mo_name]['parameter'][vsdata_dict[i_status]['name']] = vsdata_dict[i_status]['value']
                dict_mo[mo_name]['id_name'][i_status] = vsdata_dict[i_status]['name']
    
    return mo_name

def clean_up_csv(path):
	for file in os.listdir(path):
		if file.endswith("csv"):
			os.remove("%s/%s" %(path, file))
		#elif file.endswith("txt"):
		#	filenames.append("%s/%s" %(path, file))
	#return filenames

def get_mo_data2(mo_name, param, data):
    if param not in dict_mo[mo_name]['header']:
        lowerheader = [x.lower() for x in dict_mo[mo_name]['header']]
        param_lower = param.lower()
        if param_lower in lowerheader:
            i = lowerheader.index(param_lower)
            param = dict_mo[mo_name]['header'][i]
        else:
            dict_mo[mo_name]['header'].append(param)
    dict_mo[mo_name]['parameter'][param] = re.sub(',',';', data) 
    
def write_mo_data(mo_name):
    sTemp = ""
    start = 0
    for key, value in dict_mo[mo_name]['parameter'].items(): 
        if start == 0:
            sTemp = value
            start = 1
        else:
            sTemp = "%s,%s" %(sTemp,value)
    sTemp = sTemp + "\n"
    dict_mo[mo_name]['io'].write(sTemp.encode('utf8'))

def get_param_name(tag):
    s1 = re.split("}", tag )
    if s1:
        param_name = s1[-1]
    else:
        param_name = tag
    param_name = param_name.strip()
    return param_name

def decode_cmbulk_xml(file, output_folder):
    bEutranCellFDD = False
    sTemp = ""
    sHeader = ""
    bHeader = False

    global list_mo 
    global dict_mo
    bwrite = False
   
    vsdata_status = 0
    vsdata_dict = {}
    print("Reading %s, start time: %s" % (file, datetime.now()))
    events = ET.iterparse(file, events=("start", "end",))
    print("read complete, time: %s" % (datetime.now()))
    
    print("Print MO, time: %s" % (datetime.now()))
    
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    id_name = 'id'
    numME = 0
    numMO = 0
    s_mo_name = 'mo_name'
    b_mo_start = False
    b_mo_data = False
    b_struct = False
    s_struct_name = ''
    b_utrancell = False
    
    #i_len_events = len(events)
    for event, elem in events:
        
        
        if event == "start": 
            if 'VsDataContainer'in elem.tag:
                
                #status vsdata hierarchy
                vsdata_status = vsdata_status * 10 + 1
                #start of MO
                if b_mo_start == True:
                    i_num_id = i_num_id + 1
                else:
                    i_num_id = 1
                b_mo_start = True
                s_mo_name = ''
                #get_id

                if 'id' in elem.attrib:
                    s_mo_id = elem.attrib["id"]
                    vsdata_dict[vsdata_status] = {'name': None, 'value':elem.attrib["id"]}
                else:
                    print("%s,%s,%s"%(elem.tag,elem.text,elem.attrib))
                    vsdata_dict[vsdata_status] = {'name': None, 'value':''}
                s_mo_id = ''
            elif 'vsDataType'in elem.tag:
                s_mo_name = elem.text
                if s_mo_name is not None:
                    #check id if already in header list or not
                    temp_id = "%sid" %(re.sub('vsData', '', s_mo_name))
                    vsdata_dict[vsdata_status]['name'] = temp_id.lower()
                    s_mo_name = check_mo(s_mo_name, s_mo_id, s_subnetwork, s_mecontext, file, vsdata_dict, vsdata_status, output_folder)
            elif '}SubNetwork'in elem.tag:
                if 'id' in elem.attrib:
                    s_subnetwork = elem.attrib["id"]
                else:
                    s_subnetwork = ''
            elif '}MeContext'in elem.tag:
                if 'id' in elem.attrib:
                    s_mecontext = elem.attrib["id"]
                    numME = numME + 1
                    if numME % 50 == 0:
                        print("processing %s MEs" %(numME))

            elif '}UtranCell'in elem.tag:
                if 'id' in elem.attrib:
                    s_utrancell = elem.attrib["id"]
                    b_utrancell = True
            else:
                if s_mo_name!= '':
                    if s_mo_name != None:
                        if s_mo_name in elem.tag:
                            b_mo_data = True
                        elif b_mo_data == True:
                            if elem.text != None:
                                if '\n' not in elem.text:
                                    param = get_param_name(elem.tag)
                                    if not b_struct:
                                        get_mo_data2(s_mo_name, param, elem.text)
                                    else:
                                        get_mo_data2(s_mo_name, s_struct_name + "_" + param, elem.text)
                                else:
                                    b_struct = True
                                    s = re.split("}", elem.tag )
                                    if s:
                                        s_struct_name = s[-1]
                                    else:
                                        s_struct_name = elem.tag
                            else:
                                b_struct = True
                                s = re.split("}", elem.tag )
                                if s:
                                    s_struct_name = s[-1]
                                else:
                                    s_struct_name = elem.tag
        elif event == "end": 
            if 'VsDataContainer'in elem.tag:
                #status vsdata hierarchy
                vsdata_status = (vsdata_status - 1)/ 10
                b_mo_start = False
                if i_num_id > 1:
                    i_num_id = i_num_id - 1
                else:
                    i_num_id = 0
            elif '}SubNetwork'in elem.tag:
                s_subnetwork = ''
            elif '}MeContext'in elem.tag:
                if 'MeContextId' not in elem.tag:
                    s_mecontext = ''
            elif '}UtranCell'in elem.tag:
                b_utrancell = False
                s_utrancell = ''
            else:
                if s_mo_name != '':
                    if s_mo_name != None:
                        if s_mo_name in elem.tag:
                            b_mo_data = False
                            write_mo_data(s_mo_name)
                        elif b_struct == True and s_struct_name in elem.tag:
                            b_struct = False
        
        elem.clear()
    #ftest.close()    

    print("jumlah mo: %s" %len(list_mo))
    for mo in list_mo:
        f = dict_mo[mo]['io']
        dict_mo[mo]['io'].close()
        f.close()
        print("mo: %s" % mo)

    #combine header and parameter
    print("combine header and parameter, time: %s" %(datetime.now()))
    #print("Output_folder: %s" %output_folder )
    #create header
    for mo in list_mo:
        #print("mo: %s" % mo)
        filename = "%s/%s.csv" % (output_folder, re.sub('vsData','', mo))
        #print(filename)
        fwrite = open(filename, "wb+")
        sTemp = ""
        for param in dict_mo[mo]['header']:
            param_1 = get_param_name(param)
            if sTemp == "":
                sTemp = param_1
            else:
                sTemp = "%s,%s" %(sTemp,param_1)
        sTemp = sTemp + "\n"
        fwrite.write(sTemp.encode('utf8'))    
        
        fname = "%s/%s_1.csv" % (output_folder, re.sub('vsData','', mo))
        fparam = open(fname, encoding="utf8")
        fwrite.write(fparam.read().encode('utf8'))
        fparam.close()
        fwrite.close()
        os.remove(fname)

    dict_mo = {}

    print("stop time: %s" % (datetime.now()))


def decode_cmbulk_xml_folder(files, output_folder):
    global NumFile
    global dict_mo
    s_mecontext = ''
    for file in files:
        NumFile = NumFile + 1
        vsdata_status = 0
        vsdata_dict = {}
        print("Reading %s, start time: %s" % (file, datetime.now()))
        events = ET.iterparse(file, events=("start", "end",))
        
        print("read complete, time: %s" % (datetime.now()))
        
        print("Print MO, time: %s" % (datetime.now()))
        #basepath = os.path.abspath('.//')
        #if basepath[-1] != '\\':
        #    basepath = basepath + '\\'
        #output_folder = basepath + 'output_2\\'
    
        #if not os.path.exists(output_folder):
        #    os.makedirs(output_folder)
    
        id_name = 'id'
        numME = 0
        s_mo_name = 'mo_name'
        b_mo_start = False
        b_mo_data = False
        b_struct = False
        s_struct_name = ''
        b_utrancell = False
        
        for event, elem in events:
            numME = numME + 1
            if event == "start": 
                if 'VsDataContainer'in elem.tag:
                    #status vsdata hierarchy
                    vsdata_status = vsdata_status * 10 + 1
                    #start of MO
                    if b_mo_start == True:
                        i_num_id = i_num_id + 1
                    else:
                        i_num_id = 1
                    b_mo_start = True
                    s_mo_name = ''
                    #get_id
    
                    if 'id' in elem.attrib:
                        s_mo_id = elem.attrib["id"]
                        vsdata_dict[vsdata_status] = {'name': None, 'value':elem.attrib["id"]}
                    else:
                        print("%s,%s,%s"%(elem.tag,elem.text,elem.attrib))
                        vsdata_dict[vsdata_status] = {'name': None, 'value':''}
                    s_mo_id = ''
                elif 'vsDataType'in elem.tag:
                    s_mo_name = elem.text
                    if s_mo_name is not None:
                        #check id if already in header list or not
                        temp_id = "%sid" %(re.sub('vsData', '', s_mo_name))
                        vsdata_dict[vsdata_status]['name'] = temp_id.lower()
                        s_mo_name = check_mo(s_mo_name, s_mo_id, s_subnetwork, s_mecontext, file, vsdata_dict, vsdata_status, output_folder)
                elif '}SubNetwork'in elem.tag:
                    if 'id' in elem.attrib:
                        s_subnetwork = elem.attrib["id"]
                    else:
                        s_subnetwork = ''
                elif '}MeContext'in elem.tag:
                    if 'id' in elem.attrib:
                        s_mecontext = elem.attrib["id"]
                elif '}UtranCell'in elem.tag:
                    if 'id' in elem.attrib:
                        s_utrancell = elem.attrib["id"]
                        b_utrancell = True
                else:
                    if s_mo_name!= '':
                        if s_mo_name != None:
                            if s_mo_name in elem.tag:
                                b_mo_data = True
                            elif b_mo_data == True:
                                if elem.text != None:
                                    if '\n' not in elem.text:
                                        param = get_param_name(elem.tag)
                                        if not b_struct:
                                            get_mo_data2(s_mo_name, param, elem.text)
                                        else:
                                            get_mo_data2(s_mo_name, s_struct_name + "_" + param, elem.text)
                                    else:
                                        b_struct = True
                                        s = re.split("}", elem.tag )
                                        if s:
                                            s_struct_name = s[-1]
                                        else:
                                            s_struct_name = elem.tag
                                else:
                                    b_struct = True
                                    s = re.split("}", elem.tag )
                                    if s:
                                        s_struct_name = s[-1]
                                    else:
                                        s_struct_name = elem.tag
            elif event == "end": 
                if 'VsDataContainer'in elem.tag:
                    #status vsdata hierarchy
                    vsdata_status = (vsdata_status - 1)/ 10
                    b_mo_start = False
                    if i_num_id > 1:
                        i_num_id = i_num_id - 1
                    else:
                        i_num_id = 0
                elif '}SubNetwork'in elem.tag:
                    s_subnetwork = ''
                elif '}MeContext'in elem.tag:
                    if 'MeContextId' not in elem.tag:
                        s_mecontext = ''
                elif '}UtranCell'in elem.tag:
                    b_utrancell = False
                    s_utrancell = ''
                else:
                    if s_mo_name != '':
                        if s_mo_name != None:
                            if s_mo_name in elem.tag:
                                b_mo_data = False
                                write_mo_data(s_mo_name)
                            elif b_struct == True and s_struct_name in elem.tag:
                                b_struct = False
            
            elem.clear()
    
    for mo in list_mo:
        f = dict_mo[mo]['io']
        dict_mo[mo]['io'].close()
        f.close()
    
    #combine header and parameter
    print("combine header and parameter, time: %s" %(datetime.now()))
    #create header
    for mo in list_mo:
        filename = "%s/%s.csv" % (output_folder, re.sub('vsData','', mo))
        fwrite = open(filename, "wb+")
        sTemp = ""
        for param in dict_mo[mo]['header']:
            param_1 = get_param_name(param)
            if sTemp == "":
                sTemp = param_1
            else:
                sTemp = "%s,%s" %(sTemp,param_1)
        sTemp = sTemp + "\n"
        fwrite.write(sTemp.encode('utf8'))    
        
        fname = "%s/%s_1.csv" % (output_folder, re.sub('vsData','', mo))
        fparam = open(fname, encoding="utf8")
        fwrite.write(fparam.read().encode('utf8'))
        fparam.close()
        fwrite.close()
        os.remove(fname)
    
    dict_mo = {}
    
    print("stop time: %s" % (datetime.now()))

def get_file_infolder_xml(path):
	filenames = []
	for file in os.listdir(path):
		if file.endswith("xml"):
			filenames.append("%s/%s" %(path, file))
	return filenames

if __name__ == '__main__':
    before = False
    if len(sys.argv) > 2:
        before = True

    python_file_path = os.path.dirname(os.path.realpath(__file__))
    
    if os.path.isfile("%s/CMBulk Import success.txt" %python_file_path):
        os.remove("%s/CMBulk Import success.txt" %python_file_path)
        
    output_folder = "%s/decoded_cm" % python_file_path
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)

    input = sys.argv[1]

    bEutranCellFDD = False
    sTemp = ""
    sHeader = ""
    bHeader = False

    NumFile = 0
    list_mo = []
    dict_mo = {}
    s_subnetwork = ''
    s_mecontext = ''
    i_num_id = 0

    id_name = 'id'
    numME = 0
    s_mo_name = 'mo_name'
    b_mo_start = False
    b_mo_data = False
    b_struct = False
    s_struct_name = ''
    b_utrancell = False
    
    if len(sys.argv) > 3:
        status = sys.argv[3]
    else:
        status = ""

    cmbulk_path_SUMBAGUT = "%s/cmbulk/%s/SUMBAGUT" %(python_file_path, status)
    cmbulk_path_SUMBAGTENG = "%s/cmbulk/%s/SUMBAGTENG" %(python_file_path, status)
    cmbulk_path_KALIMANTAN = "%s/cmbulk/%s/KALIMANTAN" %(python_file_path, status)

    cmbulk_before_path_SUMBAGUT = "%s/cmbulk/%s/SUMBAGUT/before" %(python_file_path, status)
    cmbulk_before_path_SUMBAGTENG = "%s/cmbulk/%s/SUMBAGTENG/before" %(python_file_path, status)
    cmbulk_before_path_KALIMANTAN = "%s/cmbulk/%s/KALIMANTAN/before" %(python_file_path, status)

    decoded_cm_path_SUMBAGUT = "%s/decoded_cm/%s/SUMBAGUT" %(python_file_path, status)
    decoded_cm_path_SUMBAGTENG = "%s/decoded_cm/%s/SUMBAGTENG" %(python_file_path, status)
    decoded_cm_path_KALIMANTAN = "%s/decoded_cm/%s/KALIMANTAN" %(python_file_path, status)

    decoded_cm_before_path_SUMBAGUT = "%s/decoded_cm/%s/SUMBAGUT/before" %(python_file_path, status)
    decoded_cm_before_path_SUMBAGTENG = "%s/decoded_cm/%s/SUMBAGTENG/before" %(python_file_path, status)
    decoded_cm_before_path_KALIMANTAN = "%s/decoded_cm/%s/KALIMANTAN/before" %(python_file_path, status)

    cmbulk_before_path = "%s/cmbulk/%s/before" %(python_file_path, status)
    decoded_cm_path = "%s/decoded_cm/%s" %(python_file_path, status)
    decoded_cm_before_path = "%s/decoded_cm/%s/before" %(python_file_path, status)

    list_region = ["SUMBAGUT", "SUMBAGTENG", "KALIMANTAN"]


    list_folder = [cmbulk_path_SUMBAGUT,cmbulk_path_SUMBAGTENG,cmbulk_path_KALIMANTAN,
                   cmbulk_before_path_SUMBAGUT, cmbulk_before_path_SUMBAGTENG, cmbulk_before_path_KALIMANTAN,
                   decoded_cm_path_SUMBAGUT, decoded_cm_path_SUMBAGTENG, decoded_cm_path_KALIMANTAN,
                   decoded_cm_before_path_SUMBAGUT, decoded_cm_before_path_SUMBAGTENG, decoded_cm_before_path_KALIMANTAN
                  ]
    list_io = [{'cm_folder':cmbulk_path_SUMBAGUT,'decoded_cm_path':decoded_cm_path_SUMBAGUT},
               {'cm_folder':cmbulk_path_SUMBAGTENG,'decoded_cm_path':decoded_cm_path_SUMBAGTENG},
               {'cm_folder':cmbulk_path_KALIMANTAN,'decoded_cm_path':decoded_cm_path_KALIMANTAN}
              ]

    prev_list = [cmbulk_before_path, decoded_cm_path, decoded_cm_before_path]

    # Creating mandatory directory
    if not os.path.exists("decoded_cm"):
        os.mkdir("decoded_cm")

    if not os.path.exists("decoded_cm/feature"):
        os.mkdir("decoded_cm/feature")

    if not os.path.exists("decoded_cm/parameter"):
        os.mkdir("decoded_cm/parameter")

    if not os.path.exists("decoded_cm/export"):
        os.mkdir("decoded_cm/export")

    for region in list_region:
        loc1 = "decoded_cm/%s/%s" %(status, region)
        if not os.path.exists(loc1):
            os.mkdir(loc1)
        for folder in list_folder:
            if not os.path.exists(folder):
                os.mkdir(folder)
            else:
                clean_up_csv(folder)
        for prev in prev_list:
            if not os.path.exists(prev):
                os.mkdir(prev)
            else:
                clean_up_csv(prev)

    '''
    for folder in list_folder:
        if not os.path.exists(folder):
            os.mkdir(folder)
        else:
            clean_up_csv(folder)
    '''

    if input == '1':
        for index in list_io:  
            filenames = get_file_infolder_xml(index['cm_folder'])
            #print('input : %s, output : %s' %(index['cm_folder'], index['decoded_cm_path']))
            bEutranCellFDD = False
            sTemp = ""
            sHeader = ""
            bHeader = False

            NumFile = 0
            list_mo = []
            dict_mo = {}
            s_subnetwork = ''
            s_mecontext = ''
            i_num_id = 0

            id_name = 'id'
            numME = 0
            s_mo_name = 'mo_name'
            b_mo_start = False
            b_mo_data = False
            b_struct = False
            s_struct_name = ''
            b_utrancell = False
            decode_cmbulk_xml_folder(filenames, index['decoded_cm_path'])
            print("finish!")
    elif input == '2':
        file = sys.argv[2]
        decode_cmbulk_xml(file, cmbulk_before_path)

    bEutranCellFDD = False
    sTemp = ""
    sHeader = ""
    bHeader = False

    NumFile = 0
    list_mo = []
    dict_mo = {}
    s_subnetwork = ''
    s_mecontext = ''
    i_num_id = 0

    id_name = 'id'
    numME = 0
    s_mo_name = 'mo_name'
    b_mo_start = False
    b_mo_data = False
    b_struct = False
    s_struct_name = ''
    b_utrancell = False

    if before:
        filenames = get_file_infolder_xml(cmbulk_before_path)
        decode_cmbulk_xml_folder(filenames, decoded_cm_before_path)

    if os.path.exists("%s/format_txt.txt" %decoded_cm_path):
        os.remove("%s/format_txt.txt" %decoded_cm_path)
		
    with open("%s/format_csv.txt" %decoded_cm_path, "w") as outfile:
        outfile.write("csv format")

    #os.chdir(python_file_path)
    #with open(f_complete, "w") as outfile:
    #    outfile.write("done")

    t2 = datetime.now()

    file1 = open("CMBulk Import success.txt","w") 
    L = t2.strftime("%d-%b-%Y (%H:%M:%S.%f)")
    file1.write(L)
    file1.close()